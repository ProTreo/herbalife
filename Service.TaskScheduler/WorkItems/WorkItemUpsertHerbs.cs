﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Module.Spreadsheets;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.Models;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Specifications;

namespace Service.TaskScheduler.WorkItems
{
    public class WorkItemUpsertHerbs : IWorkItemUpsertHerbs
    {
        private const string CacheKeyAllHerbs = "CACHE_KEY_ALL_HERBS";
        private const string CantCreateNewHerbErrorCode = "CANT_CREATE_NEW_HERB";
        private const string CantUpdateExistingHerbError = "CANT_UPDATE_EXISTING_HERB";
        private const string UnknownErrorCode = "UNKNOWN_ERROR";
        private const string MessageUpsertHerbsFailed = "Возникли ошибки при обновлении базы растений.";

        private readonly IHerbRepository _herbRepository;
        private readonly IMemoryCache _memoryCache;

        private readonly Func<Herb, string> _cantCreateNewHerbErrorDescription = herb => $"Возникла проблема при создании растения {herb.Name}" +
                                                                                         $"с латинскими названиями:{Environment.NewLine}" +
                                                                                         $"{herb.LatinNames.Select(hln => hln.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                         $"и русскими синонимами:{Environment.NewLine}" +
                                                                                         $"{herb.RussianSynonyms.Select(hrs => hrs.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}";

        private readonly GoogleSpreadsheetsImporter2 _googleSpreadsheetsImporter;

        public WorkItemUpsertHerbs(GoogleSpreadsheetsImporter2 googleSpreadsheetsImporter, IHerbRepository herbRepository, IMemoryCache memoryCache)
        {
            _googleSpreadsheetsImporter = googleSpreadsheetsImporter;
            _herbRepository = herbRepository;
            _memoryCache = memoryCache;
        }

        private Func<Herb, Herb, Exception, string> _cantUpdateExistingHerbErrorDescription => (existingHerb, newHerb, ex) => $"Возникла ошибка при обновлении растения:{Environment.NewLine}" +
                                                                                                                              "Старые данные:" +
                                                                                                                              $"Название: {existingHerb.Name}{Environment.NewLine}" +
                                                                                                                              $"Латинские названия:{Environment.NewLine}" +
                                                                                                                              $"{existingHerb.LatinNames.Select(hln => hln.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                              $"Русские синонимы:{Environment.NewLine}" +
                                                                                                                              $"{existingHerb.RussianSynonyms.Select(hrs => hrs.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                              $"{Environment.NewLine}" +
                                                                                                                              "Новые данные:" +
                                                                                                                              $"Название: {newHerb.Name}{Environment.NewLine}" +
                                                                                                                              $"Латинские названия:{Environment.NewLine}" +
                                                                                                                              $"{newHerb.LatinNames.Select(hln => hln.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                              $"Русские синонимы:{Environment.NewLine}" +
                                                                                                                              $"{newHerb.RussianSynonyms.Select(hrs => hrs.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                              $"{Environment.NewLine}" +
                                                                                                                              $"Ошибка:{Environment.NewLine}" +
                                                                                                                              $"{ex}";

        private Func<Exception, string> _unknownErrorDescription => ex => $"Возникла ошибка:{Environment.NewLine}" +
                                                                          $"{ex}";

        public async Task<IWorkItemResult> Execute()
        {
            await _googleSpreadsheetsImporter.Import();
            var newHerbs = _googleSpreadsheetsImporter.Herbs;
            var workItemErrors = new List<Error>();

            var specification = new HerbWithSourcesAllSpecification();
            var herbs = await _memoryCache.GetOrCreateAsync(CacheKeyAllHerbs, cacheEntry =>
            {
                cacheEntry.SlidingExpiration = TimeSpan.FromMinutes(10);
                cacheEntry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(30);
                cacheEntry.Priority = CacheItemPriority.High;

                return _herbRepository.List(specification);
            });

            foreach (var newHerb in newHerbs)
            {
                var existingHerb = herbs.FirstOrDefault(h => h.Name.Equals(newHerb.Name));
                var upsertError = await TryUpsertHerb(newHerb, existingHerb);

                if (!(upsertError is null))
                {
                    workItemErrors.Add(upsertError);
                }
            }

            return new WorkItemResult(!workItemErrors.Any(), workItemErrors);
        }

        private async Task<Error> TryUpsertHerb(Herb newHerb, Herb existingHerb)
        {
            // Если растения нету - пробуем создать
            if (existingHerb is null)
            {
                var creationError = await CreateNewHerb(newHerb);

                if (!(creationError is null))
                {
                    return creationError;
                }
            }
            // Обновляем существующее растение
            else
            {
                var updateError = await UpdateHerb(newHerb, existingHerb);

                if (!(updateError is null))
                {
                    return updateError;
                }
            }

            return null;
        }

        private async Task<Error> CreateNewHerb(Herb newHerb)
        {
            try
            {
                var addedHerb = await _herbRepository.Add(newHerb);

                if (!(addedHerb is null))
                {
                    return new Error(CantCreateNewHerbErrorCode, _cantCreateNewHerbErrorDescription(newHerb));
                }
            }
            catch (Exception ex)
            {
                return new Error(UnknownErrorCode, _unknownErrorDescription(ex));
            }

            return null;
        }

        private async Task<Error> UpdateHerb(Herb newHerb, Herb existingHerb)
        {
            try
            {
                existingHerb.Name = newHerb.Name;
                UpdateHerbLatinNames(newHerb, existingHerb);
                UpdateHerbRussianSynonyms(newHerb, existingHerb);
                UpdateHerbParts(newHerb, existingHerb);

                await _herbRepository.Update(existingHerb);
            }
            catch (Exception ex)
            {
                return new Error(CantUpdateExistingHerbError, _cantUpdateExistingHerbErrorDescription(existingHerb, newHerb, ex));
            }

            return null;
        }

        private static void UpdateHerbParts(Herb newHerb, Herb existingHerb)
        {
            existingHerb.Parts = newHerb.Parts;
        }

        private static void UpdateHerbRussianSynonyms(Herb newHerb, Herb existingHerb)
        {
            existingHerb.RussianSynonyms = newHerb.RussianSynonyms;
        }

        private static void UpdateHerbLatinNames(Herb newHerb, Herb existingHerb)
        {
            foreach (var herbLatinName in newHerb.LatinNames)
            {
                if (existingHerb.LatinNames.All(hln => hln.Name != herbLatinName.Name))
                {
                    //existingHerb.LatinNames.Add(new HerbLatinName // TODO Попробовать через UpdateHerbsUseCase чтобы не нарушать безопасность internal -> public
                    //{
                    //    Herb = existingHerb,
                    //    Name = herbLatinName.Name
                    //});
                }
            }
            existingHerb.LatinNames = newHerb.LatinNames;
        }
    }
}