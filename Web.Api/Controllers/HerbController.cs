﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Models.Request;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api")]
    [ApiController]
    public class HerbController : ControllerBase
    {
        private readonly SearchHerbsPresenter _searchHerbsPresenter;
        private readonly ISearchHerbsUseCase _searchHerbsUseCase;

        public HerbController(ISearchHerbsUseCase searchHerbsUseCase, SearchHerbsPresenter searchHerbsPresenter)
        {
            _searchHerbsUseCase = searchHerbsUseCase;
            _searchHerbsPresenter = searchHerbsPresenter;
        }

        // POST api/herb/search
        [HttpPost("search")]
        public async Task<IActionResult> Search([FromBody] SearchHerbsRequest request)
        {
            // client validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _searchHerbsUseCase.Handle(new Core.Dto.UseCaseRequests.SearchHerbsRequest(request.Query, ModelState),
                _searchHerbsPresenter);

            // remote validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            return _searchHerbsPresenter.ContentResult;
        }

        // POST api/ping
        /// <summary>
        ///     Метод нужен для подтверждения аутентификации на при любом запросе страницы
        /// </summary>
        /// <returns></returns>
        [HttpPost("ping")]
        public IActionResult Ping()
        {
            return Ok();
        }
    }
}