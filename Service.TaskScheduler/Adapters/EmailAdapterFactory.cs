﻿using Microsoft.Extensions.Options;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.Models;

namespace Service.TaskScheduler.Adapters
{
    internal class EmailAdapterFactory : IEmailAdapterFactory
    {
        private readonly MailSettings _mailSettings;

        public EmailAdapterFactory(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }
        public IEmailAdapter GetEmailAdapter()
        {
            return new PostfixAdapter(_mailSettings);
        }
    }
}