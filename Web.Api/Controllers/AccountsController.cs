﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Models.Request;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly RegisterUserPresenter _registerUserPresenter;
        private readonly IRegisterUserUseCase _registerUserUseCase;

        public AccountsController(IRegisterUserUseCase registerUserUseCase, RegisterUserPresenter registerUserPresenter)
        {
            _registerUserUseCase = registerUserUseCase;
            _registerUserPresenter = registerUserPresenter;
        }

        // POST api/register
        [HttpPost("register")]
        public async Task<ActionResult> Post([FromBody] RegisterUserRequest request)
        {
            // client validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _registerUserUseCase.Handle(
                new Core.Dto.UseCaseRequests.RegisterUserRequest(request.Email, request.Password,
                    request.IsPersonalDataProcessingAgreementReceived, ModelState),
                _registerUserPresenter);

            // remote validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            return _registerUserPresenter.ContentResult;
        }
    }
}