﻿using System;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.GatewayResponses.Repositories;
using Web.Api.Core.Interfaces.Gateways.Repositories;

namespace Web.Api.Infrastructure.Data.Repositories
{
    public class HerbAdditionRepository : EfRepository<HerbAddition>, IHerbAdditionRepository
    {
        public HerbAdditionRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<CreateHerbAdditionResponse> Create(User user, string message)
        {
            var response = await Add(new HerbAddition(user, message));

            return new CreateHerbAdditionResponse(response.Id.ToString(), true);
        }

        public async Task<CreateHerbAdditionResponse> Create(string message)
        {
            var response = await Add(new HerbAddition(message));

            return new CreateHerbAdditionResponse(response.Id.ToString(), true);
        }
    }
}