﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_chemical_source_to_herb_source")]
    public class HerbChemicalSourceToHerbSource : BaseEntity
    {
        internal HerbChemicalSourceToHerbSource()
        {
        }

        [JsonIgnore]
        public virtual HerbChemicalSource ChemicalSource { get; set; }
        public int HerbChemicalSourceId { get; set; }

        public virtual HerbSource Source { get; set; }
        public int HerbSourceId { get; set; }
    }
}