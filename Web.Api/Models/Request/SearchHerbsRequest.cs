﻿namespace Web.Api.Models.Request
{
    public class SearchHerbsRequest
    {
        public string Query { get; set; }
    }
}