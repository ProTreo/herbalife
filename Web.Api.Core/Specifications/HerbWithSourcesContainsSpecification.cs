﻿using System.Linq;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class HerbWithSourcesContainsSpecification : BaseSpecification<Herb>
    {
        public HerbWithSourcesContainsSpecification(string query) : base(h =>
            h.Name.Contains(query) ||
            h.LatinNames.Any(hln => hln.Name.Contains(query)) ||
            h.RussianSynonyms.Any(hrs => hrs.Name.Contains(query)))
        {
            AddInclude(h => h.LatinNames);
            AddInclude(h => h.RussianSynonyms);
            AddInclude(h => h.Parts);
            AddInclude(h => h.Parts);
            AddInclude("Parts.Type");
            AddInclude("Parts.ChemicalSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources.Source");
        }
    }
}