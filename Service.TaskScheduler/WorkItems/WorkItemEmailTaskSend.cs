﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.Models;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Domain.Entities.Feedback;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Specifications;

namespace Service.TaskScheduler.WorkItems
{
    public class WorkItemEmailTaskSend : IWorkItemEmailTaskSend
    {
        private readonly IEmailAdapter _emailAdapter;
        private readonly IEmailTasksRepository _emailTasksRepository;
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IHerbAdditionRepository _herbAdditionRepository;

        private readonly Func<List<Error>, string> _aggregatedErrors = errors => errors.Select(e => e.Description).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}");

        public WorkItemEmailTaskSend(IEmailAdapterFactory emailAdapterFactory,
            IEmailTasksRepository emailTasksRepository,
            IFeedbackRepository feedbackRepository,
            IHerbAdditionRepository herbAdditionRepository)
        {
            _emailAdapter = emailAdapterFactory.GetEmailAdapter();

            _emailTasksRepository = emailTasksRepository;
            _feedbackRepository = feedbackRepository;
            _herbAdditionRepository = herbAdditionRepository;
        }

        /// <summary>
        ///     Основная логика задания.
        /// </summary>
        /// <returns></returns>
        public async Task<IWorkItemResult> Execute()
        {
            var emailTasks = await _emailTasksRepository.List(new GetEmailTasksSpecification(), 5);
            var workItemErrors = new List<Error>();

            // Проставляем статус Queue, чтобы не забирать эти же задания второй раз
            foreach (var et in emailTasks)
            {
                et.Status = EmailTaskStatus.Queue;
                await _emailTasksRepository.Update(et);

                var emailSubject = GetSubject(et);
                var isSent = _emailAdapter.Send(new EmailData(et.Message, await emailSubject, et.Recipients), out var sendErrors);

                // Проставляет Статус Error/Done в зависимости от результата выполнения
                if (!isSent)
                {
                    workItemErrors.AddRange(sendErrors);

                    et.Status = EmailTaskStatus.Error;
                    et.StatusMessage = _aggregatedErrors(sendErrors);
                }
                else
                {
                    et.Status = EmailTaskStatus.Done;
                }

                await _emailTasksRepository.Update(et);
            }

            return new WorkItemResult(!workItemErrors.Any(), workItemErrors);
        }

        /// <summary>
        ///     Получает тему сообщения.
        /// </summary>
        /// <param name="emailTask">Почтовое задание.</param>
        /// <returns></returns>
        private async Task<string> GetSubject(EmailTask emailTask)
        {
            string subject;

            switch (emailTask.Type)
            {
                case EmailType.Email:
                    subject = "Автоматическое почтовое уведомление";
                    break;
                case EmailType.Feedback:
                    subject = await GetFeedbackSubject(emailTask.EntityId);
                    break;
                case EmailType.HerbAddition:
                    subject = await GetHerbAdditionSubject(emailTask.EntityId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(emailTask));
            }

            return subject;
        }

        /// <summary>
        ///     Получает тему сообщения для почтового задания - Обратная связь.
        /// </summary>
        /// <param name="entityId">ИД сущности - Обратная связь.</param>
        /// <returns></returns>
        private async Task<string> GetFeedbackSubject(int entityId)
        {
            //var feedback = await _feedbackRepository.GetById(entityId);
            var feedback = await _feedbackRepository.GetSingleBySpec(new GetFeedbackByIdSpecification(entityId));

            return $"Сообщение обратной связи{(feedback.User is null ? string.Empty : $" от {feedback.User.Email}")}.";
        }
        
        /// <summary>
        ///     Получает тему сообщения для почтового задания - Запрос на добавление растения.
        /// </summary>
        /// <param name="entityId">ИД сущности - Запрос на добавление растения.</param>
        /// <returns></returns>
        private async Task<string> GetHerbAdditionSubject(int entityId)
        {
            //var herbAddition = await _herbAdditionRepository.GetById(entityId);
            var herbAddition = await _herbAdditionRepository.GetSingleBySpec(new GetHerbAdditionByIdSpecification(entityId));

            return $"Запрос на добавление растения{(herbAddition.User is null ? string.Empty : $" от {herbAddition.User.Email}")}.";
        }
    }
}