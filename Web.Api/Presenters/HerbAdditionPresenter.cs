﻿using System.Net;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Serialization;

namespace Web.Api.Presenters
{
    public class HerbAdditionPresenter : IOutputPort<HerbAdditionResponse>
    {
        public HerbAdditionPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public JsonContentResult ContentResult { get; }

        public void Handle(HerbAdditionResponse response)
        {
            ContentResult.StatusCode = (int) (response.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            ContentResult.Content = response.Success
                ? JsonSerializer.SerializeObject(response.Id)
                : JsonSerializer.SerializeObject(response.Message);
        }
    }
}