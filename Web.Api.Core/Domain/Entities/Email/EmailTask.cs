﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Email
{
    [Table("email_task")]
    public class EmailTask : BaseEntity
    {
        internal EmailTask()
        {
        }

        internal EmailTask(string message, IEnumerable<string> recipients, int entityId,
            EmailType type = EmailType.Email, EmailTaskStatus status = EmailTaskStatus.Ready)
        {
            Message = message;
            Recipients = recipients;
            EntityId = entityId;
            Type = type;
            Status = status;
        }

        public string Message { get; private set; }

        public IEnumerable<string> Recipients { get; private set; }
        public int EntityId { get; private set; }
        public EmailType Type { get; private set; }
        public EmailTaskStatus Status { get; set; }
        public string StatusMessage { get; set; }
    }
}