﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Service.TaskScheduler.Interfaces
{
    public interface IBackgroundTaskQueue
    {
        /// <summary>
        /// Ставит в очередь задание
        /// </summary>
        /// <param name="workItem"></param>
        void QueueBackgroundWorkItem(Func<CancellationToken, IWorkItem> workItem);

        /// <summary>
        /// Снимает задание из очереди
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Func<CancellationToken, IWorkItem>> DequeueAsync(CancellationToken cancellationToken);
    }
}