﻿using System.Reflection;
using Autofac;
using Module.Spreadsheets;
using Service.TaskScheduler.Adapters;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.Services;
using Service.TaskScheduler.WorkItems;

namespace Service.TaskScheduler
{
    public class TaskSchedulerModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Фаблики
            builder.RegisterType<EmailAdapterFactory>().As<IEmailAdapterFactory>().SingleInstance();

            // Extra
            builder.RegisterType<GoogleSpreadsheetsImporter2>().AsSelf().SingleInstance();

            // Presenters
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(t => t.Name.EndsWith("Presenter")).SingleInstance();

            // Рабочие юниты
            builder.RegisterType<WorkItemEmailTaskSend>().As<IWorkItemEmailTaskSend>().InstancePerDependency();
            builder.RegisterType<WorkItemUpsertHerbsUsingUseCase>().As<IWorkItemUpsertHerbs>().InstancePerDependency();

            // Сервисы
            builder.RegisterType<EmailSenderLoop>().AsSelf().SingleInstance();
            builder.RegisterType<DatabaseOperationsLoop>().AsSelf().SingleInstance();
            builder.RegisterType<QueuedHostedService>().AsSelf().SingleInstance();
            builder.RegisterType<BackgroundTaskQueue>().As<IBackgroundTaskQueue>().SingleInstance();
        }
    }
}