﻿using System.Threading.Tasks;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.Services;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public sealed class LoginUseCase : ILoginUseCase
    {
        private readonly IJwtFactory _jwtFactory;
        private readonly ITokenFactory _tokenFactory;
        private readonly IUserRepository _userRepository;

        public LoginUseCase(IUserRepository userRepository, IJwtFactory jwtFactory, ITokenFactory tokenFactory)
        {
            _userRepository = userRepository;
            _jwtFactory = jwtFactory;
            _tokenFactory = tokenFactory;
        }

        public async Task<bool> Handle(LoginRequest message, IOutputPort<LoginResponse> outputPort)
        {
            var user = await _userRepository.FindByEmail(message.Email);

            if (user == null)
            {
                message.ModelState.AddModelError(nameof(message.Email), "Почта не зарегистрирована");
                return false;
            }

            if (!await _userRepository.CheckPassword(user, message.Password))
            {
                message.ModelState.AddModelError(nameof(message.Password), "Неправильный пароль");
                return false;
            }

            // generate refresh token
            var refreshToken = _tokenFactory.GenerateToken();
            user.AddRefreshToken(refreshToken, user.Id, message.RemoteIpAddress);
            await _userRepository.Update(user);

            // generate access token
            outputPort.Handle(new LoginResponse(await _jwtFactory.GenerateEncodedToken(user.IdentityId, user.Email),
                refreshToken, true));
            return true;
        }
    }
}