﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Core;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.Specifications;

namespace Web.Api.Core.UseCases
{
    public class UpdateHerbsUseCase : IUpdateHerbsUseCase
    {
        private const string CacheKeyAllHerbs = "CACHE_KEY_ALL_HERBS";
        private const string CacheKeyAllHerbSources = "CACHE_KEY_ALL_HERB_SOURCES";
        private const string CacheKeyAllHerbPartTypes = "CACHE_KEY_ALL_HERB_PART_TYPES";
        private const string CantCreateNewHerbErrorCode = "CANT_CREATE_NEW_HERB";
        private const string CantUpdateExistingHerbError = "CANT_UPDATE_EXISTING_HERB";
        private const string UnknownErrorCode = "UNKNOWN_ERROR";
        private const string MessageUpsertHerbsFailed = "Возникли ошибки при обновлении базы растений.";

        private readonly Func<Herb, string> _cantCreateNewHerbErrorDescription = herb => $"Возникла проблема при создании растения {herb.Name}" +
                                                                                         $"с латинскими названиями:{Environment.NewLine}" +
                                                                                         $"{herb.LatinNames.Select(hln => hln.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                         $"и русскими синонимами:{Environment.NewLine}" +
                                                                                         $"{herb.RussianSynonyms.Select(hrs => hrs.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}";

        private readonly Func<Herb, Herb, Exception, string> _cantUpdateExistingHerbErrorDescription = (existingHerb, newHerb, ex) => $"Возникла ошибка при обновлении растения:{Environment.NewLine}" +
                                                                                                                                      "Старые данные:" +
                                                                                                                                      $"Название: {existingHerb.Name}{Environment.NewLine}" +
                                                                                                                                      $"Латинские названия:{Environment.NewLine}" +
                                                                                                                                      $"{existingHerb.LatinNames.Select(hln => hln.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                                      $"Русские синонимы:{Environment.NewLine}" +
                                                                                                                                      $"{existingHerb.RussianSynonyms.Select(hrs => hrs.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                                      $"{Environment.NewLine}" +
                                                                                                                                      "Новые данные:" +
                                                                                                                                      $"Название: {newHerb.Name}{Environment.NewLine}" +
                                                                                                                                      $"Латинские названия:{Environment.NewLine}" +
                                                                                                                                      $"{newHerb.LatinNames.Select(hln => hln.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                                      $"Русские синонимы:{Environment.NewLine}" +
                                                                                                                                      $"{newHerb.RussianSynonyms.Select(hrs => hrs.Name).Aggregate((p, n) => $"{p}{Environment.NewLine}{n}")}" +
                                                                                                                                      $"{Environment.NewLine}" +
                                                                                                                                      $"Ошибка:{Environment.NewLine}" +
                                                                                                                                      $"{ex}";

        private readonly IHerbRepository _herbRepository;

        private readonly Func<Exception, string> _unknownErrorDescription = ex => $"Возникла ошибка:{Environment.NewLine}" +
                                                                                  $"{ex}";

        private ICollection<HerbPartType> _existingHerbPartTypes;
        private ICollection<Herb> _existingHerbs;
        private ICollection<HerbSource> _existingHerbSources;

        private IEnumerable<HerbPartType> _newHerbPartTypes;
        private IEnumerable<Herb> _newHerbs;
        private IEnumerable<HerbSource> _newHerbSources;

        public UpdateHerbsUseCase(IHerbRepository herbRepository)
        {
            _herbRepository = herbRepository;
        }

        public async Task<bool> Handle(UpdateHerbsRequest message, IOutputPort<UpdateHerbsResponse> outputPort)
        {
            _newHerbSources = message.HerbSources;
            _newHerbPartTypes = message.HerbPartTypes;
            _newHerbs = message.Herbs;

            var specification = new HerbWithSourcesAllSpecification();

            _herbRepository.RemoveAllTrackedEntities();
            _existingHerbs = await _herbRepository.List(specification);
            _existingHerbSources = await _herbRepository.ListHerbSources();
            _existingHerbPartTypes = await _herbRepository.ListHerbPartTypes();

            var upsertErrors = new List<Error>();
            foreach (var newHerb in _newHerbs)
            {
                var existingHerb = _existingHerbs.FirstOrDefault(h => h.Name == newHerb.Name);
                var upsertError = await TryUpsertHerb(newHerb, existingHerb);

                if (!(upsertError is null))
                {
                    upsertErrors.Add(upsertError);
                }
            }

            var notActualHerbs = _existingHerbs.Where(eh => _newHerbs.All(nh => nh.Name != eh.Name)).ToList();

            foreach (var notActualHerb in notActualHerbs)
            {
                await _herbRepository.Delete(notActualHerb);
            }

            outputPort.Handle(upsertErrors.Any() ? new UpdateHerbsResponse(upsertErrors, message: MessageUpsertHerbsFailed) : new UpdateHerbsResponse(true));

            return true;
        }

        private async Task<Error> TryUpsertHerb(Herb newHerb, Herb existingHerb)
        {
            // Если растения нету - пробуем создать
            if (existingHerb is null)
            {
                var creationError = await CreateNewHerb(newHerb);

                if (!(creationError is null))
                {
                    return creationError;
                }
            }
            // Обновляем существующее растение
            else
            {
                var updateError = await UpdateHerb(newHerb, existingHerb);

                if (!(updateError is null))
                {
                    return updateError;
                }
            }

            return null;
        }

        /// <summary>
        ///     Создает растение.
        /// </summary>
        /// <param name="newHerb">Новое растение.</param>
        /// <returns></returns>
        private async Task<Error> CreateNewHerb(Herb newHerb)
        {
            try
            {
                var herb = new Herb
                {
                    Name = newHerb.Name
                };

                var existingHerb = await _herbRepository.Add(herb);

                if (existingHerb is null)
                {
                    return new Error(CantCreateNewHerbErrorCode, _cantCreateNewHerbErrorDescription(newHerb));
                }

                await UpdateHerb(newHerb, existingHerb);
            }
            catch (Exception ex)
            {
                return new Error(UnknownErrorCode, _unknownErrorDescription(ex));
            }

            return null;
        }

        /// <summary>
        ///     Обновляет растение.
        /// </summary>
        /// <param name="newHerb">Новое растение.</param>
        /// <param name="existingHerb">Старое растение.</param>
        /// <returns></returns>
        private async Task<Error> UpdateHerb(Herb newHerb, Herb existingHerb)
        {
            try
            {
                existingHerb.Name = newHerb.Name;

                UpdateHerbLatinNames(newHerb, existingHerb);
                UpdateHerbRussianSynonyms(newHerb, existingHerb);
                UpdateHerbParts(newHerb, existingHerb);

                await _herbRepository.Update(existingHerb);
            }
            catch (Exception ex)
            {
                return new Error(CantUpdateExistingHerbError, _cantUpdateExistingHerbErrorDescription(existingHerb, newHerb, ex));
            }

            return null;
        }

        /// <summary>
        ///     Обновляет список частей растения.
        /// </summary>
        /// <param name="newHerb">Новое растение.</param>
        /// <param name="existingHerb">Старое растение.</param>
        private void UpdateHerbParts(Herb newHerb, Herb existingHerb)
        {
            foreach (var newHerbPart in newHerb.Parts)
            {
                var existingHerbPart = existingHerb.Parts.SingleOrDefault(hp => hp.Type.Name == newHerbPart.Type.Name);
                var existingHerbPartType = _existingHerbPartTypes.SingleOrDefault(hpt => hpt.Name == newHerbPart.Type.Name);
                var newHerbPartType = _newHerbPartTypes.SingleOrDefault(hpt => hpt.Name == newHerbPart.Type.Name);

                if (existingHerbPart is null)
                {
                    existingHerbPart = new HerbPart
                    {
                        Herb = existingHerb,
                        Type = existingHerbPartType ?? newHerbPartType,
                        AlkaloidsMaxPercent = newHerbPart.AlkaloidsMaxPercent,
                        AlkaloidsMinPercent = newHerbPart.AlkaloidsMinPercent,
                        AlkaloidsPresence = newHerbPart.AlkaloidsPresence,
                        MucilageMaxPercent = newHerbPart.MucilageMaxPercent,
                        MucilageMinPercent = newHerbPart.MucilageMinPercent,
                        MucilagePresence = newHerbPart.MucilagePresence,
                        NutrientsMaxPercent = newHerbPart.NutrientsMaxPercent,
                        NutrientsMinPercent = newHerbPart.NutrientsMinPercent,
                        NutrientsPresence = newHerbPart.NutrientsPresence,
                        TanninsMaxPercent = newHerbPart.TanninsMaxPercent,
                        TanninsMinPercent = newHerbPart.TanninsMinPercent,
                        TanninsPresence = newHerbPart.TanninsPresence
                    };

                    existingHerb.Parts.Add(existingHerbPart);
                }
                else
                {
                    existingHerbPart.Herb = existingHerb;
                    existingHerbPart.Type = existingHerbPartType ?? newHerbPartType;
                    existingHerbPart.AlkaloidsMaxPercent = newHerbPart.AlkaloidsMaxPercent;
                    existingHerbPart.AlkaloidsMinPercent = newHerbPart.AlkaloidsMinPercent;
                    existingHerbPart.AlkaloidsPresence = newHerbPart.AlkaloidsPresence;
                    existingHerbPart.MucilageMaxPercent = newHerbPart.MucilageMaxPercent;
                    existingHerbPart.MucilageMinPercent = newHerbPart.MucilageMinPercent;
                    existingHerbPart.MucilagePresence = newHerbPart.MucilagePresence;
                    existingHerbPart.NutrientsMaxPercent = newHerbPart.NutrientsMaxPercent;
                    existingHerbPart.NutrientsMinPercent = newHerbPart.NutrientsMinPercent;
                    existingHerbPart.NutrientsPresence = newHerbPart.NutrientsPresence;
                    existingHerbPart.TanninsMaxPercent = newHerbPart.TanninsMaxPercent;
                    existingHerbPart.TanninsMinPercent = newHerbPart.TanninsMinPercent;
                    existingHerbPart.TanninsPresence = newHerbPart.TanninsPresence;
                }

                UpdateHerbPartChemicalSources(newHerbPart, existingHerbPart);
            }
        }

        /// <summary>
        ///     Обновляет источники химических веществ.
        /// </summary>
        /// <param name="newHerbPart">Новая часть растения.</param>
        /// <param name="existingHerbPart">Существующая часть растения.</param>
        private void UpdateHerbPartChemicalSources(HerbPart newHerbPart, HerbPart existingHerbPart)
        {
            UpdateHerbPartChemicalSource(newHerbPart, existingHerbPart, HerbChemicalType.Alkaloids);
            UpdateHerbPartChemicalSource(newHerbPart, existingHerbPart, HerbChemicalType.Mucilage);
            UpdateHerbPartChemicalSource(newHerbPart, existingHerbPart, HerbChemicalType.Nutrients);
            UpdateHerbPartChemicalSource(newHerbPart, existingHerbPart, HerbChemicalType.Tannins);
        }

        /// <summary>
        ///     Обновляет источники конкретного химического вещества.
        /// </summary>
        /// <param name="newHerbPart">Новая часть растения.</param>
        /// <param name="existingHerbPart">Существующая часть растения.</param>
        /// <param name="chemicalType">Тип химического вещества.</param>
        private void UpdateHerbPartChemicalSource(HerbPart newHerbPart, HerbPart existingHerbPart, HerbChemicalType chemicalType)
        {
            var existingChemicalSource = existingHerbPart.ChemicalSources.SingleOrDefault(cs => cs.ChemicalType == chemicalType);
            var newChemicalSource = newHerbPart.ChemicalSources.SingleOrDefault(cs => cs.ChemicalType == chemicalType);

            if (newChemicalSource is null)
            {
                return;
            }

            if (existingChemicalSource is null)
            {
                existingChemicalSource = new HerbChemicalSource
                {
                    ChemicalType = chemicalType,
                    Part = existingHerbPart
                };

                existingHerbPart.ChemicalSources.Add(existingChemicalSource);
            }

            UpdateChemicalSourcesToSource(newChemicalSource, existingChemicalSource);
        }

        /// <summary>
        ///     Обновляет таблицу соотношения источников химического вещества к источнику информации.
        /// </summary>
        /// <param name="newChemicalSource">Новый химический источник.</param>
        /// <param name="existingChemicalSource">Старый химический источник.</param>
        private void UpdateChemicalSourcesToSource(HerbChemicalSource newChemicalSource, HerbChemicalSource existingChemicalSource)
        {
            try
            {
                foreach (var newHerbChemicalSourceToHerbSource in newChemicalSource.ChemicalSourcesToSources)
                {
                    var existingHerbChemicalSourceToHerbSource =
                        existingChemicalSource.ChemicalSourcesToSources.SingleOrDefault(cs =>
                            cs.Source.Name == newHerbChemicalSourceToHerbSource.Source.Name &&
                            cs.Source.Author == newHerbChemicalSourceToHerbSource.Source.Author &&
                            cs.Source.Isbn == newHerbChemicalSourceToHerbSource.Source.Isbn &&
                            cs.Source.Type == newHerbChemicalSourceToHerbSource.Source.Type &&
                            cs.Source.Year == newHerbChemicalSourceToHerbSource.Source.Year);

                    if (!(existingHerbChemicalSourceToHerbSource is null))
                    {
                        continue;
                    }

                    var existingHerbSource = _existingHerbSources.SingleOrDefault(ehs => ehs.Name == newHerbChemicalSourceToHerbSource.Source.Name &&
                                                                                         ehs.Author == newHerbChemicalSourceToHerbSource.Source.Author &&
                                                                                         ehs.Isbn == newHerbChemicalSourceToHerbSource.Source.Isbn &&
                                                                                         ehs.Type == newHerbChemicalSourceToHerbSource.Source.Type &&
                                                                                         ehs.Year == newHerbChemicalSourceToHerbSource.Source.Year);

                    if (existingHerbSource is null)
                    {
                        existingHerbSource = new HerbSource
                        {
                            Name = newHerbChemicalSourceToHerbSource.Source.Name,
                            Author = newHerbChemicalSourceToHerbSource.Source.Author,
                            Isbn = newHerbChemicalSourceToHerbSource.Source.Isbn,
                            Type = newHerbChemicalSourceToHerbSource.Source.Type,
                            Year = newHerbChemicalSourceToHerbSource.Source.Year
                        };

                        _existingHerbSources.Add(existingHerbSource);
                    }

                    existingHerbChemicalSourceToHerbSource = new HerbChemicalSourceToHerbSource
                    {
                        ChemicalSource = existingChemicalSource,
                        Source = existingHerbSource
                    };
                    
                    existingChemicalSource.ChemicalSourcesToSources.Add(existingHerbChemicalSourceToHerbSource);

                    var notActualHerbChemicalSourceToHerbSources = existingChemicalSource.ChemicalSourcesToSources.Where(
                            ecsts => newChemicalSource.ChemicalSourcesToSources.All(ncsts =>
                                ncsts.Source.Name != ecsts.Source.Name &&
                                ncsts.Source.Author != ecsts.Source.Author &&
                                ncsts.Source.Isbn != ecsts.Source.Isbn &&
                                ncsts.Source.Type != ecsts.Source.Type &&
                                ncsts.Source.Year != ecsts.Source.Year))
                        .ToList();

                    foreach (var notActualHerbChemicalSourceToHerbSource in notActualHerbChemicalSourceToHerbSources)
                    {
                        existingChemicalSource.ChemicalSourcesToSources.Remove(notActualHerbChemicalSourceToHerbSource);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        ///     Обновляет список русских синонимов растения.
        /// </summary>
        /// <param name="newHerb">Новое растение.</param>
        /// <param name="existingHerb">Старое растение.</param>
        private void UpdateHerbRussianSynonyms(Herb newHerb, Herb existingHerb)
        {
            foreach (var newHerbRussianSynonym in newHerb.RussianSynonyms)
            {
                var existingHerbRussianSynonym = existingHerb.RussianSynonyms.SingleOrDefault(rs => rs.Name == newHerbRussianSynonym.Name);

                if (!(existingHerbRussianSynonym is null))
                {
                    continue;
                }

                existingHerbRussianSynonym = new HerbRussianSynonym
                {
                    Herb = existingHerb,
                    Name = newHerbRussianSynonym.Name
                };

                existingHerb.RussianSynonyms.Add(existingHerbRussianSynonym);
            }

            var notActualHerbRussianSynonyms = existingHerb.RussianSynonyms.Where(ers => newHerb.RussianSynonyms.All(nrs => nrs.Name != ers.Name)).ToList();

            foreach (var notActualHerbRussianSynonym in notActualHerbRussianSynonyms)
            {
                existingHerb.RussianSynonyms.Remove(notActualHerbRussianSynonym);
            }
        }

        /// <summary>
        ///     Обновляет список латинских имен растения.
        /// </summary>
        /// <param name="newHerb">Новое растение.</param>
        /// <param name="existingHerb">Старое растение.</param>
        private void UpdateHerbLatinNames(Herb newHerb, Herb existingHerb)
        {
            foreach (var newHerbLatinName in newHerb.LatinNames)
            {
                var existingHerbLatinName = existingHerb.LatinNames.SingleOrDefault(rs => rs.Name == newHerbLatinName.Name);

                if (!(existingHerbLatinName is null))
                {
                    continue;
                }

                existingHerbLatinName = new HerbLatinName
                {
                    Herb = existingHerb,
                    Name = newHerbLatinName.Name
                };

                existingHerb.LatinNames.Add(existingHerbLatinName);
            }

            var notActualHerbLatinNames = existingHerb.LatinNames.Where(ers => newHerb.LatinNames.All(nrs => nrs.Name != ers.Name)).ToList();

            foreach (var notActualHerbLatinName in notActualHerbLatinNames)
            {
                existingHerb.LatinNames.Remove(notActualHerbLatinName);
            }
        }
    }
}