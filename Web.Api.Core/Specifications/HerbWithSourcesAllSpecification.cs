﻿using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class HerbWithSourcesAllSpecification : BaseSpecification<Herb>
    {
        public HerbWithSourcesAllSpecification() : base(h => true)
        {
            AddInclude(h => h.LatinNames);
            AddInclude(h => h.RussianSynonyms);
            AddInclude(h => h.Parts);
            AddInclude(h => h.Parts);
            AddInclude("Parts.Type");
            AddInclude("Parts.ChemicalSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources.Source");
        }
    }
}