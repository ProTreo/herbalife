﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class RegisterUserRequest : BaseRequest, IUseCaseRequest<RegisterUserResponse>
    {
        public RegisterUserRequest(string email, string password, bool isPersonalDataProcessingAgreementReceived,
            ModelStateDictionary modelState)
        {
            Email = email;
            Password = password;
            IsPersonalDataProcessingAgreementReceived = isPersonalDataProcessingAgreementReceived;
            ModelState = modelState;
        }

        public string Email { get; }
        public string Password { get; }
        public bool IsPersonalDataProcessingAgreementReceived { get; }
    }
}