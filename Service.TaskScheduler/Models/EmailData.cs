﻿using System.Collections.Generic;

namespace Service.TaskScheduler.Models
{
    public class EmailData
    {
        public EmailData(string message, string subject, IEnumerable<string> recipients)
        {
            Message = message;
            Subject = subject;
            Recipients = recipients;
        }

        public string Message { get; private set; }
        public string Subject { get; private set; }
        public IEnumerable<string> Recipients { get; private set; }
    }
}