﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_part")]
    public class HerbPart : BaseEntity
    {
        internal HerbPart()
        {
            ChemicalSources = new List<HerbChemicalSource>();
        }

        [JsonIgnore]
        public virtual Herb Herb { get; set; }
        public int HerbId { get; set; }

        public HerbPartType Type { get; set; }
        public int TypeId { get; set; }

        public virtual ICollection<HerbChemicalSource> ChemicalSources { get; set; }

        public bool? AlkaloidsPresence { get; set; }
        public double? AlkaloidsMinPercent { get; set; }
        public double? AlkaloidsMaxPercent { get; set; }
        public bool? MucilagePresence { get; set; }
        public double? MucilageMinPercent { get; set; }
        public double? MucilageMaxPercent { get; set; }
        public bool? TanninsPresence { get; set; }
        public double? TanninsMinPercent { get; set; }
        public double? TanninsMaxPercent { get; set; }
        public bool? NutrientsPresence { get; set; }
        public double? NutrientsMinPercent { get; set; }
        public double? NutrientsMaxPercent { get; set; }
    }
}
