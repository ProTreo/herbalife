﻿using System.Linq;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class HerbWithSourcesStartsWithSpecification : BaseSpecification<Herb>
    {
        public HerbWithSourcesStartsWithSpecification(string query) : base(h =>
            h.Name.StartsWith(query) || h.LatinNames.Any(hln => hln.Name.StartsWith(query)) ||
            h.RussianSynonyms.Any(hrs => hrs.Name.StartsWith(query)))
        {
            AddInclude(h => h.LatinNames);
            AddInclude(h => h.RussianSynonyms);
            AddInclude(h => h.Parts);
            AddInclude(h => h.Parts);
            AddInclude("Parts.Type");
            AddInclude("Parts.ChemicalSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources.Source");
        }
    }
}