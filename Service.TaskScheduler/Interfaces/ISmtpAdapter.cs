﻿using System.Collections.Generic;
using Service.TaskScheduler.Models;

namespace Service.TaskScheduler.Interfaces
{
    public interface IEmailAdapter
    {
        bool Send(EmailData emailData, out List<Error> errors);
    }
}