﻿using System.Collections.Generic;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class UpdateHerbsRequest : BaseRequest, IUseCaseRequest<UpdateHerbsResponse>
    {
        public UpdateHerbsRequest(IEnumerable<HerbSource> herbSources, IEnumerable<HerbPartType> herbPartTypes, IEnumerable<Herb> herbs)
        {
            HerbSources = herbSources;
            HerbPartTypes = herbPartTypes;
            Herbs = herbs;
        }

        public IEnumerable<HerbSource> HerbSources { get; }
        public IEnumerable<HerbPartType> HerbPartTypes { get; }
        public IEnumerable<Herb> Herbs { get; }
    }
}