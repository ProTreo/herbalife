﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Module.Spreadsheets.Models;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Module.Spreadsheets
{
    public class GoogleSpreadsheetsImporter2
    {
        private const string ApplicationName = "Herbalife";
        private const string HerbPartTypesRange = "'Части растений'!A:A";
        private const string HerbSourcesRange = "'Источники'!A:F";
        private const string HerbRange = "'Малая бд'!A:L";
        private const string SpreadsheetId = "1UkvE9sD_nH1fpXb1zDq77pDcqbES1syLC-1dN-UQFjE";

        private readonly Func<string, string> _errorGettingHerbPartTypes = range => $"Невозможно получить части растений для диапазона {range}";
        private readonly Func<string, string> _errorGettingHerbs = range => $"Невозможно получить растений для диапазона {range}";
        private readonly Func<string, string> _errorGettingHerbSources = range => $"Невозможно получить источники растений для диапазона {range}";
        private readonly Func<string, Exception, string> _errorImport = (herbName, ex) => $"Ошибка при получении данных из гугл таблиц, растение: {herbName}, ошибка: {ex}";

        private readonly SheetsService _service;
        private readonly string _credentialsPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "credentials.json");

        public GoogleSpreadsheetsImporter2()
        {
            _service = GetService();
        }

        public List<HerbPartType> HerbPartTypes { get; set; }
        public List<HerbSource> HerbSources { get; set; }
        public List<Herb> Herbs { get; private set; }

        public async Task Import()
        {
            HerbPartTypes = await GetHerbPartTypes();
            HerbSources = await GetHerbSources();
            Herbs = new List<Herb>();

            var request = _service.Spreadsheets.Values.Get(SpreadsheetId, HerbRange);
            var response = await request.ExecuteAsync();
            var values = response.Values;

            if (values == null || values.Count == 0)
            {
                throw new NullReferenceException(_errorGettingHerbs(HerbRange));
            }

            foreach (var value in values.Skip(2))
            {
                var herbRow = HerbRow.FromObject(value);

                Herb herb;

                if (Herbs.Any() && (string.IsNullOrEmpty(herbRow.Name) || Herbs.Any(h => h.Name == herbRow.Name)))
                {
                    herb = string.IsNullOrEmpty(herbRow.Name) ? Herbs.Last() : Herbs.Find(h => h.Name == herbRow.Name);
                }
                else
                {
                    herb = new Herb
                    {
                        Name = herbRow.Name
                    };

                    Herbs.Add(herb);
                }

                // Латинские имена
                if (herbRow.LatinNames.Any())
                {
                    foreach (var latinName in herbRow.LatinNames)
                    {
                        herb.LatinNames.Add(new HerbLatinName
                        {
                            Herb = herb,
                            Name = latinName
                        });
                    }
                }

                // Русские синонимы
                if (herbRow.RussianSynonyms.Any())
                {
                    foreach (var russianSynonym in herbRow.RussianSynonyms)
                    {
                        herb.RussianSynonyms.Add(new HerbRussianSynonym
                        {
                            Herb = herb,
                            Name = russianSynonym
                        });
                    }
                }

                if (herbRow.PartName == string.Empty)
                {
                    herbRow.PartName = HerbPartTypes[0].Name;
                }

                var herbPart = new HerbPart
                {
                    Herb = herb,
                    Type = HerbPartTypes.Find(hpt => hpt.Name == herbRow.PartName),
                    AlkaloidsPresence = herbRow.AlkaloidsPresence,
                    AlkaloidsMinPercent = herbRow.AlkaloidsMinPercent,
                    AlkaloidsMaxPercent = herbRow.AlkaloidsMaxPercent,
                    MucilagePresence = herbRow.MucilagePresence,
                    MucilageMinPercent = herbRow.MucilageMinPercent,
                    MucilageMaxPercent = herbRow.MucilageMaxPercent,
                    TanninsPresence = herbRow.TanninsPresence,
                    TanninsMinPercent = herbRow.TanninsMinPercent,
                    TanninsMaxPercent = herbRow.TanninsMaxPercent,
                    NutrientsPresence = herbRow.NutrientsPresence,
                    NutrientsMinPercent = herbRow.NutrientsMinPercent,
                    NutrientsMaxPercent = herbRow.NutrientsMaxPercent,
                    ChemicalSources = new List<HerbChemicalSource>()
                };

                // Алкалоиды
                if (herbRow.AlkaloidsSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        ChemicalType = HerbChemicalType.Alkaloids,
                        ChemicalSourcesToSources = new List<HerbChemicalSourceToHerbSource>(),
                        Part = herbPart
                    };

                    foreach (var source in herbRow.AlkaloidsSources)
                    {
                        chemicalSource.ChemicalSourcesToSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            ChemicalSource = chemicalSource,
                            Source = HerbSources[source - 1]
                        });
                    }

                    herbPart.ChemicalSources.Add(chemicalSource);
                }

                // Клей
                if (herbRow.MucilageSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        ChemicalType = HerbChemicalType.Mucilage,
                        ChemicalSourcesToSources = new List<HerbChemicalSourceToHerbSource>(),
                        Part = herbPart
                    };

                    foreach (var source in herbRow.MucilageSources)
                    {
                        chemicalSource.ChemicalSourcesToSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            ChemicalSource = chemicalSource,
                            Source = HerbSources[source - 1]
                        });
                    }

                    herbPart.ChemicalSources.Add(chemicalSource);
                }

                // Дубильные
                if (herbRow.TanninsSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        ChemicalType = HerbChemicalType.Tannins,
                        ChemicalSourcesToSources = new List<HerbChemicalSourceToHerbSource>(),
                        Part = herbPart
                    };

                    foreach (var source in herbRow.TanninsSources)
                    {
                        chemicalSource.ChemicalSourcesToSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            ChemicalSource = chemicalSource,
                            Source = HerbSources[source - 1]
                        });
                    }

                    herbPart.ChemicalSources.Add(chemicalSource);
                }

                // Питательные
                if (herbRow.NutrientsSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        ChemicalType = HerbChemicalType.Nutrients,
                        ChemicalSourcesToSources = new List<HerbChemicalSourceToHerbSource>(),
                        Part = herbPart
                    };

                    foreach (var source in herbRow.NutrientsSources)
                    {
                        chemicalSource.ChemicalSourcesToSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            ChemicalSource = chemicalSource,
                            Source = HerbSources[source - 1]
                        });
                    }

                    herbPart.ChemicalSources.Add(chemicalSource);
                }

                try
                {
                    herb.Parts.Add(herbPart);
                }
                catch (Exception ex)
                {
                    throw new Exception(_errorImport(herb.Name, ex));
                }
            }
        }

        private async Task<List<HerbSource>> GetHerbSources()
        {
            var request = _service.Spreadsheets.Values.Get(SpreadsheetId, HerbSourcesRange);
            var response = await request.ExecuteAsync();
            var values = response.Values;

            if (values == null || values.Count == 0)
            {
                throw new NullReferenceException(_errorGettingHerbSources(HerbSourcesRange));
            }

            return values.Skip(1).Select((row, index) => new HerbSource
            {
                Type = row[1].ToString(),
                Author = row[2].ToString(),
                Name = row[3].ToString(),
                Year = int.Parse(row[4].ToString()),
                Isbn = row[5].ToString()
            }).ToList();
        }

        private async Task<List<HerbPartType>> GetHerbPartTypes()
        {
            var request = _service.Spreadsheets.Values.Get(SpreadsheetId, HerbPartTypesRange);
            var response = await request.ExecuteAsync();
            var values = response.Values;

            if (values == null || !values.Any())
            {
                throw new NullReferenceException(_errorGettingHerbPartTypes(HerbPartTypesRange));
            }

            return values.Select((row, index) =>
                new HerbPartType
                {
                    Name = row[0].ToString()
                }).ToList();
        }

        private SheetsService GetService()
        {
            var scopes = new[] { SheetsService.Scope.SpreadsheetsReadonly };

            GoogleCredential credential;

            using (var stream = new FileStream(_credentialsPath, FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream).CreateScoped(scopes);
            }

            return new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName
            });
        }
    }
}