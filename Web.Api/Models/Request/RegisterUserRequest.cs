﻿namespace Web.Api.Models.Request
{
    public class RegisterUserRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsPersonalDataProcessingAgreementReceived { get; set; }
    }
}