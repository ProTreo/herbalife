﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Web.Api.Serialization
{
    public sealed class JsonSerializer
    {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            ContractResolver = new JsonContractResolver(),
            NullValueHandling = NullValueHandling.Include,
            //ReferenceLoopHandling = ReferenceLoopHandling.Ignore // Глобальное игнорирование цикличных свойств Prop1 -> Prop2.Prop1
        };

        public static string SerializeObject(object o)
        {
            return JsonConvert.SerializeObject(o, Formatting.Indented, Settings);
        }

        public sealed class JsonContractResolver : CamelCasePropertyNamesContractResolver
        {
        }
    }
}