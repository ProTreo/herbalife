﻿namespace Service.TaskScheduler.Interfaces
{
    public interface IEmailAdapterFactory
    {
        IEmailAdapter GetEmailAdapter();
    }
}