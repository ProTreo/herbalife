﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class HerbAdditionRequest : BaseRequest, IUseCaseRequest<HerbAdditionResponse>
    {
        public HerbAdditionRequest(string email, string message, ModelStateDictionary modelState)
        {
            Email = email;
            Message = message;
            ModelState = modelState;
        }

        public string Email { get; }
        public string Message { get; }
    }
}