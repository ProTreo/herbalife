﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using Module.Spreadsheets.Models;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Module.Spreadsheets
{
    public class GoogleSpreadsheetsImporter
    {
        public GoogleSpreadsheetsImporter()
        {
            Herbs = new List<Herb>();
            HerbLatinNames = new List<HerbLatinName>();
            HerbRussianSynonyms = new List<HerbRussianSynonym>();
            HerbParts = new List<HerbPart>();
            HerbPartTypes = new List<HerbPartType>();
            HerbSources = new List<HerbSource>();
            HerbChemicalSources = new List<HerbChemicalSource>();
            HerbChemicalSourcesToHerbSources = new List<HerbChemicalSourceToHerbSource>();
        }

        public List<Herb> Herbs { get; private set; }
        public List<HerbLatinName> HerbLatinNames { get; private set; }
        public List<HerbRussianSynonym> HerbRussianSynonyms { get; private set; }
        public List<HerbPart> HerbParts { get; private set; }
        public List<HerbPartType> HerbPartTypes { get; private set; }
        public List<HerbSource> HerbSources { get; private set; }
        public List<HerbChemicalSource> HerbChemicalSources { get; set; }
        public List<HerbChemicalSourceToHerbSource> HerbChemicalSourcesToHerbSources { get; set; }

        [Obsolete]
        public void Import(string credentialsPath, string spreadsheetId)
        {
            var scopes = new[] { SheetsService.Scope.SpreadsheetsReadonly };
            var applicationName = "Herbalife";

            UserCredential credential;

            using (var stream = new FileStream(credentialsPath, FileMode.Open, FileAccess.Read))
            {
                var credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets, scopes, "user",
                    CancellationToken.None, new FileDataStore(credPath, true)).Result;
            }

            var service = new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = applicationName
            });

            #region HerbPartType

            var range = "'Части растений'!A:A";
            var request = service.Spreadsheets.Values.Get(spreadsheetId, range);
            var response = request.Execute();
            var values = response.Values;

            if (values == null || values.Count == 0)
            {
                throw new NullReferenceException("Невозможно получить части растений");
            }

            HerbPartTypes = values.Select((row, index) => new HerbPartType { Id = index + 1, Name = row[0].ToString() })
                .ToList();

            #endregion

            #region HerbSource

            range = "'Источники'!A:F";
            request = service.Spreadsheets.Values.Get(spreadsheetId, range);
            response = request.Execute();
            values = response.Values;

            if (values == null || values.Count == 0)
            {
                throw new NullReferenceException("Невозможно получить источники");
            }

            HerbSources = values.Skip(1).Select((row, index) => new HerbSource
                {
                    Id = index + 1,
                    Type = row[1].ToString(),
                    Author = row[2].ToString(),
                    Name = row[3].ToString(),
                    Year = int.Parse(row[4].ToString()),
                    Isbn = row[5].ToString()
                }
            ).ToList();

            #endregion

            #region HerbParts, LatinNames, RussianSynonyms, HerbChemicalSources, Herbs

            //range = "'Малая бд (тест)'";
            range = "'Малая бд'!A1:L146";
            request = service.Spreadsheets.Values.Get(spreadsheetId, range);
            response = request.Execute();
            values = response.Values;

            if (values == null || values.Count == 0)
            {
                throw new NullReferenceException("Невозможно получить данные из малой бд");
            }

            foreach (var value in values.Skip(2)) // Пропускаем первые 2 строчки, т.к. это заголовки
            {
                var herbRow = HerbRow.FromObject(value);

                Herb herb;

                if (Herbs.Any() && (herbRow.Name == string.Empty || herbRow.Name == Herbs.Last().Name))
                {
                    herb = Herbs.Last();
                }
                else
                {
                    herb = new Herb
                    {
                        Id = Herbs.Count + 1,
                        Name = herbRow.Name
                    };

                    Herbs.Add(herb);
                }

                if (herbRow.LatinNames.Any())
                {
                    foreach (var latinName in herbRow.LatinNames)
                        HerbLatinNames.Add(new HerbLatinName
                        {
                            Id = HerbLatinNames.Count + 1,
                            HerbId = herb.Id,
                            Name = latinName
                        });
                }

                if (herbRow.RussianSynonyms.Any())
                {
                    foreach (var russianSynonym in herbRow.RussianSynonyms)
                        HerbRussianSynonyms.Add(new HerbRussianSynonym
                        {
                            Id = HerbRussianSynonyms.Count + 1,
                            HerbId = herb.Id,
                            Name = russianSynonym
                        });
                }

                if (herbRow.PartName == string.Empty)
                {
                    herbRow.PartName = HerbPartTypes[0].Name;
                }

                var herbPart = new HerbPart
                {
                    Id = HerbParts.Count + 1,
                    HerbId = herb.Id,
                    TypeId = HerbPartTypes.First(hpt => hpt.Name == herbRow.PartName).Id,
                    AlkaloidsPresence = herbRow.AlkaloidsPresence,
                    AlkaloidsMinPercent = herbRow.AlkaloidsMinPercent,
                    AlkaloidsMaxPercent = herbRow.AlkaloidsMaxPercent,
                    MucilagePresence = herbRow.MucilagePresence,
                    MucilageMinPercent = herbRow.MucilageMinPercent,
                    MucilageMaxPercent = herbRow.MucilageMaxPercent,
                    TanninsPresence = herbRow.TanninsPresence,
                    TanninsMinPercent = herbRow.TanninsMinPercent,
                    TanninsMaxPercent = herbRow.TanninsMaxPercent,
                    NutrientsPresence = herbRow.NutrientsPresence,
                    NutrientsMinPercent = herbRow.NutrientsMinPercent,
                    NutrientsMaxPercent = herbRow.NutrientsMaxPercent
                };

                if (herbRow.AlkaloidsSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        Id = HerbChemicalSources.Count + 1,
                        ChemicalType = HerbChemicalType.Alkaloids,
                        HerbPartId = herbPart.Id
                    };

                    foreach (var source in herbRow.AlkaloidsSources)
                    {
                        HerbChemicalSourcesToHerbSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            Id = HerbChemicalSourcesToHerbSources.Count + 1,
                            HerbChemicalSourceId = chemicalSource.Id,
                            HerbSourceId = source
                        });
                    }

                    HerbChemicalSources.Add(chemicalSource);
                }

                if (herbRow.MucilageSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        Id = HerbChemicalSources.Count + 1,
                        ChemicalType = HerbChemicalType.Mucilage,
                        HerbPartId = herbPart.Id
                    };

                    foreach (var source in herbRow.MucilageSources)
                    {
                        HerbChemicalSourcesToHerbSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            Id = HerbChemicalSourcesToHerbSources.Count + 1,
                            HerbChemicalSourceId = chemicalSource.Id,
                            HerbSourceId = source
                        });
                    }

                    HerbChemicalSources.Add(chemicalSource);
                }

                if (herbRow.TanninsSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        Id = HerbChemicalSources.Count + 1,
                        ChemicalType = HerbChemicalType.Tannins,
                        HerbPartId = herbPart.Id
                    };

                    foreach (var source in herbRow.TanninsSources)
                    {
                        HerbChemicalSourcesToHerbSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            Id = HerbChemicalSourcesToHerbSources.Count + 1,
                            HerbChemicalSourceId = chemicalSource.Id,
                            HerbSourceId = source
                        });
                    }

                    HerbChemicalSources.Add(chemicalSource);
                }

                if (herbRow.NutrientsSources.Any())
                {
                    var chemicalSource = new HerbChemicalSource
                    {
                        Id = HerbChemicalSources.Count + 1,
                        ChemicalType = HerbChemicalType.Nutrients,
                        HerbPartId = herbPart.Id
                    };

                    foreach (var source in herbRow.NutrientsSources)
                    {
                        HerbChemicalSourcesToHerbSources.Add(new HerbChemicalSourceToHerbSource
                        {
                            Id = HerbChemicalSourcesToHerbSources.Count + 1,
                            HerbChemicalSourceId = chemicalSource.Id,
                            HerbSourceId = source
                        });
                    }

                    HerbChemicalSources.Add(chemicalSource);
                }

                HerbParts.Add(herbPart);
                //HerbParts.Add(new Part(HerbPartTypes.First(hpt => hpt.Name == herbRow.PartName), herbRow.AlkaloidsPresence,
                //herb.Parts.Add(herbPart); // Не заполнять, иначе падает ошибка при миграции
            }

            #endregion
        }
    }
}