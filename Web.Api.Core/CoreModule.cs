﻿using Autofac;
using Microsoft.Extensions.Caching.Memory;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.UseCases;

namespace Web.Api.Core
{
    public class CoreModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MemoryCache>().As<IMemoryCache>().SingleInstance();
            builder.RegisterType<RegisterUserUseCase>().As<IRegisterUserUseCase>().InstancePerDependency();
            builder.RegisterType<LoginUseCase>().As<ILoginUseCase>().InstancePerDependency();
            builder.RegisterType<ExchangeRefreshTokenUseCase>().As<IExchangeRefreshTokenUseCase>().InstancePerDependency();
            //builder.RegisterType<SearchHerbsUseCase>().As<ISearchHerbsUseCase>().InstancePerLifetimeScope();
            //builder.RegisterType<SearchHerbsWithSourcesUseCase>().As<ISearchHerbsUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<SearchHerbsWithSourcesByMatchUseCase>().As<ISearchHerbsUseCase>().InstancePerDependency();
            builder.RegisterType<FeedbackUseCase>().As<IFeedbackUseCase>().InstancePerDependency();
            builder.RegisterType<HerbAdditionUseCase>().As<IHerbAdditionUseCase>().InstancePerDependency();
            builder.RegisterType<UpdateHerbsUseCase>().As<IUpdateHerbsUseCase>().InstancePerDependency();
        }
    }
}