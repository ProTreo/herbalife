﻿using System.Linq;
using System.Threading.Tasks;
using Module.Spreadsheets;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.Models;
using Service.TaskScheduler.Presenters;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Interfaces.UseCases;

namespace Service.TaskScheduler.WorkItems
{
    public class WorkItemUpsertHerbsUsingUseCase : IWorkItemUpsertHerbs
    {
        public const string WorkItemErrorCode = "WORK_ITEM_ERROR_CODE";
        private readonly GoogleSpreadsheetsImporter2 _googleSpreadsheetsImporter;
        private readonly UpdateHerbsPresenter _updateHerbsPresenter;
        private readonly IUpdateHerbsUseCase _updateHerbsUseCase;

        public WorkItemUpsertHerbsUsingUseCase(IUpdateHerbsUseCase updateHerbsUseCase, GoogleSpreadsheetsImporter2 googleSpreadsheetsImporter, UpdateHerbsPresenter updateHerbsPresenter)
        {
            _updateHerbsUseCase = updateHerbsUseCase;
            _googleSpreadsheetsImporter = googleSpreadsheetsImporter;
            _updateHerbsPresenter = updateHerbsPresenter;
        }

        public async Task<IWorkItemResult> Execute()
        {
            var workItemResult = new WorkItemResult(true);
            await _googleSpreadsheetsImporter.Import();
            var message = new UpdateHerbsRequest(_googleSpreadsheetsImporter.HerbSources, _googleSpreadsheetsImporter.HerbPartTypes, _googleSpreadsheetsImporter.Herbs);

            await _updateHerbsUseCase.Handle(message, _updateHerbsPresenter);

            if (!_updateHerbsPresenter.Success)
            {
                workItemResult = new WorkItemResult(false, _updateHerbsPresenter.Errors.Select(e => new Error(e.Code, e.Description)));
            }

            return workItemResult;
        }
    }
}