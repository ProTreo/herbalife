﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Moq;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Specifications;
using Web.Api.Core.UseCases;
using Xunit;

namespace Web.Api.Core.UnitTests.UseCases
{
    public class SearchHerbsUseCaseUnitTest
    {
        [Fact]
        public async void Handle_GivenEmptyQuery_ShouldReturnAllHerbs()
        {
            var mockHerbRepository = new Mock<IHerbRepository>();

            mockHerbRepository.Setup(repo => repo.List(It.IsAny<HerbStartsWithSpecification>(), It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(new List<Herb>());

            var useCase = new SearchHerbsUseCase(mockHerbRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<SearchHerbsResponse>>();

            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<SearchHerbsResponse>()));

            var response = await useCase.Handle(new SearchHerbsRequest("", new ModelStateDictionary()), mockOutputPort.Object);

            Assert.True(response);
        }

        [Fact]
        public async void Handle_GivenVeryBigQuery_ShouldReturnNoHerbs()
        {
            var mockHerbRepository = new Mock<IHerbRepository>();

            mockHerbRepository.Setup(repo => repo.List(It.IsAny<HerbStartsWithSpecification>(), It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(new List<Herb>());

            var useCase = new SearchHerbsUseCase(mockHerbRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<SearchHerbsResponse>>();

            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<SearchHerbsResponse>()));

            var response = await useCase.Handle(new SearchHerbsRequest("sdafj32riovjijvvdsv892", new ModelStateDictionary()), mockOutputPort.Object);

            Assert.True(response);
        }
    }
}