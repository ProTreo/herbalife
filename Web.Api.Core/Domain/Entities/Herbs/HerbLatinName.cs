﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_latin_name")]
    public class HerbLatinName : BaseEntity
    {
        internal HerbLatinName()
        {
        }

        public string Name { get; set; }

        [JsonIgnore]
        public virtual Herb Herb { get; set; }
        public int HerbId { get; set; }
    }
}