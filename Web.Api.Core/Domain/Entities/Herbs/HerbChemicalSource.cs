﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_chemical_source")]
    public class HerbChemicalSource : BaseEntity
    {
        internal HerbChemicalSource()
        {
            ChemicalSourcesToSources = new List<HerbChemicalSourceToHerbSource>();
        }

        public HerbChemicalType ChemicalType { get; set; }

        [JsonIgnore]
        public virtual HerbPart Part { get; set; }
        public int HerbPartId { get; set; }

        public virtual ICollection<HerbChemicalSourceToHerbSource> ChemicalSourcesToSources { get; set; }
    }
}