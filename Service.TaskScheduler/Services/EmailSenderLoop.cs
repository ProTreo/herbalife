﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Service.TaskScheduler.Interfaces;

namespace Service.TaskScheduler.Services
{
    internal class EmailSenderLoop
    {
        private readonly CancellationToken _cancellationToken;

        private readonly IWorkItem _emailTaskSend;
        private readonly ILogger _logger;
        private readonly IBackgroundTaskQueue _taskQueue;

        public EmailSenderLoop(IBackgroundTaskQueue taskQueue, ILogger<EmailSenderLoop> logger,
            IHostApplicationLifetime applicationLifetime, IWorkItemEmailTaskSend workItemEmailTaskSend)
        {
            _taskQueue = taskQueue;
            _logger = logger;
            _cancellationToken = applicationLifetime.ApplicationStopping;
            _emailTaskSend = workItemEmailTaskSend;
        }

        public void Start()
        {
            _logger.LogInformation("Email Sender Loop is starting.");

            Task.Run(SendEmail, _cancellationToken);
        }

        public async Task SendEmail()
        {
            while (!_cancellationToken.IsCancellationRequested)
            {
                _taskQueue.QueueBackgroundWorkItem(token => _emailTaskSend);

                await Task.Delay(TimeSpan.FromMinutes(1), _cancellationToken);
            }
        }
    }
}