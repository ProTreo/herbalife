﻿namespace Web.Api.Models.Response
{
    public class FeedbackResponse
    {
        public FeedbackResponse(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}