﻿using System.Linq;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class HerbStartsWithSpecification : BaseSpecification<Herb>
    {
        public HerbStartsWithSpecification(string query) : base(h =>
            h.Name.StartsWith(query) || h.LatinNames.Any(hln => hln.Name.StartsWith(query)) ||
            h.RussianSynonyms.Any(hrs => hrs.Name.StartsWith(query)))
        {
            AddInclude(h => h.LatinNames);
            AddInclude(h => h.RussianSynonyms);
            AddInclude(h => h.Parts);
            AddInclude("Parts.Type");
        }
    }
}