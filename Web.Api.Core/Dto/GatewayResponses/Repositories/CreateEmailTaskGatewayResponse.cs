﻿using System.Collections.Generic;


namespace Web.Api.Core.Dto.GatewayResponses.Repositories
{
    public class CreateEmailTaskGatewayResponse : BaseGatewayResponse
    {
        public CreateEmailTaskGatewayResponse(string id, bool success = false, IEnumerable<Error> errors = null) : base(
            success,
            errors)
        {
            Id = id;
        }

        public string Id { get; }
    }
}