﻿namespace Web.Api.Core.Domain.Entities.Herbs
{
    public enum HerbChemicalType
    {
        Default = 0,
        Alkaloids = 1,
        Mucilage = 2,
        Tannins = 3,
        Nutrients = 4
    }
}