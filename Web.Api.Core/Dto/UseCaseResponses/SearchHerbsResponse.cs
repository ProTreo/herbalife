﻿using System.Collections.Generic;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class SearchHerbsResponse : UseCaseResponseMessage
    {
        public SearchHerbsResponse(IEnumerable<Herb> herbs = null, bool success = false, string message = null) : base(success, message)
        {
            Herbs = herbs;
        }

        public IEnumerable<Herb> Herbs { get; }
    }
}