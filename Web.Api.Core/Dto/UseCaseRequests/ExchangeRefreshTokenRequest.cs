﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class ExchangeRefreshTokenRequest : BaseRequest, IUseCaseRequest<ExchangeRefreshTokenResponse>
    {
        public ExchangeRefreshTokenRequest(string accessToken, string refreshToken, string signingKey, ModelStateDictionary modelState)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            SigningKey = signingKey;
            ModelState = modelState;
        }

        public string AccessToken { get; }
        public string RefreshToken { get; }
        public string SigningKey { get; }
    }
}