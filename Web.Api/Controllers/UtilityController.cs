﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Models.Request;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api")]
    [ApiController]
    public class UtilityController : ControllerBase
    {
        private readonly IFeedbackUseCase _feedbackUseCase;
        private readonly FeedbackPresenter _feedbackPresenter;
        private readonly IHerbAdditionUseCase _herbAdditionUseCase;
        private readonly HerbAdditionPresenter _herbAdditionPresenter;

        public UtilityController(IFeedbackUseCase feedbackUseCase,
            FeedbackPresenter feedbackPresenter,
            IHerbAdditionUseCase herbAdditionUseCase,
            HerbAdditionPresenter herbAdditionPresenter)
        {
            _feedbackUseCase = feedbackUseCase;
            _feedbackPresenter = feedbackPresenter;
            _herbAdditionUseCase = herbAdditionUseCase;
            _herbAdditionPresenter = herbAdditionPresenter;
        }

        [HttpPost("feedback")]
        public async Task<IActionResult> Feedback([FromBody] FeedbackRequest request)
        {
            // client validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //await _feedbackUseCase.Handle(new Core.Dto.UseCaseRequests.FeedbackRequest(request.User, request.Message, ModelState), _feedbackPresenter);
            await _feedbackUseCase.Handle(
                new Core.Dto.UseCaseRequests.FeedbackRequest(request.Email, request.Message, ModelState),
                _feedbackPresenter);

            // remote validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            return _feedbackPresenter.ContentResult;
        }

        [HttpPost("herb_addition")]
        public async Task<IActionResult> HerbAddition([FromBody] HerbAdditionRequest request)
        {
            // client validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _herbAdditionUseCase.Handle(
                new Core.Dto.UseCaseRequests.HerbAdditionRequest(request.Email, request.Message, ModelState), 
                _herbAdditionPresenter);

            // remote validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            return _herbAdditionPresenter.ContentResult;
        }
    }
}