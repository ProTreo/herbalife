﻿using Module.StringComparison.Adapters;

namespace Module.StringComparison.Factories
{
    public interface IStringComparisonFactory
    {
        IStringComparisonAdapter GetJaroWinkler();
        IStringComparisonAdapter GetNGram(int n);
    }
}