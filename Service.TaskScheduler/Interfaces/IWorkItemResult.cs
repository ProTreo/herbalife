﻿using System.Collections.Generic;
using Service.TaskScheduler.Models;

namespace Service.TaskScheduler.Interfaces
{
    public interface IWorkItemResult
    {
        bool Success { get; }
        IEnumerable<Error> Errors { get; }
    }
}