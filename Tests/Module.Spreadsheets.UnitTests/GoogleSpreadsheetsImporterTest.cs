﻿using Xunit;

namespace Module.Spreadsheets.UnitTests
{
    public class GoogleSpreadsheetsImporterTest
    {
        [Fact]
        public void Read()
        {
            var importer = new GoogleSpreadsheetsImporter();
            var credentialsPath = "client_id.json";
            importer.Import(credentialsPath, "1UkvE9sD_nH1fpXb1zDq77pDcqbES1syLC-1dN-UQFjE");

            Assert.NotNull(importer.Herbs);
            Assert.NotNull(importer.HerbLatinNames);
            Assert.NotNull(importer.HerbRussianSynonyms);
            Assert.NotNull(importer.HerbParts);
            Assert.NotNull(importer.HerbPartTypes);
            Assert.NotNull(importer.HerbChemicalSources);
            Assert.NotNull(importer.HerbSources);
        }
    }
}