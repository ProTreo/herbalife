﻿using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.GatewayResponses.Repositories;

namespace Web.Api.Core.Interfaces.Gateways.Repositories
{
    public interface IHerbAdditionRepository : IRepository<HerbAddition>
    {
        Task<CreateHerbAdditionResponse> Create(User user, string message);
        Task<CreateHerbAdditionResponse> Create(string message);
    }
}