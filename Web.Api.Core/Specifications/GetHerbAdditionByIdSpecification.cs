﻿using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class GetHerbAdditionByIdSpecification : BaseSpecification<HerbAddition>
    {
        public GetHerbAdditionByIdSpecification(int id) : base(f => f.Id == id)
        {
            AddInclude(f => f.User);
        }
    }
}