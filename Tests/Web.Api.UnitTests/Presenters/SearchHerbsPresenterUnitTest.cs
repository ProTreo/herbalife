﻿using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Models.Validation;
using Web.Api.Presenters;
using Xunit;

namespace Web.Api.UnitTests.Presenters
{
    public class SearchHerbsPresenterUnitTest
    {
        [Fact]
        public void Handle_GivenFailedUseCaseResponse_SetsErrors()
        {
            // arrange
            var presenter = new SearchHerbsPresenter();

            // act
            presenter.Handle(new SearchHerbsResponse(null, false, "Bad request."));

            // assert
            dynamic data = JsonConvert.DeserializeObject(presenter.ContentResult.Content);
            Assert.Equal((int)HttpStatusCode.BadRequest, presenter.ContentResult.StatusCode);
            Assert.Equal("Bad request.", data);
        }

        [Fact]
        public void Handle_GivenSuccessUseCaseResponse_ReturnsSingleData()
        {
            // arrange
            var presenter = new SearchHerbsPresenter();

            // act
            presenter.Handle(new SearchHerbsResponse(new List<Herb>
            {
                new Herb
                {
                    Name = "herb name",
                    LatinNames = new List<HerbLatinName>
                    {
                        new HerbLatinName{Name = "latin_name"}
                    },
                    RussianSynonyms = new List<HerbRussianSynonym>(new[]
                    {
                        new HerbRussianSynonym{Name = "russian synonym"}
                    }),
                    Parts = new List<HerbPart>(new[]
                    {
                        new HerbPart
                        {
                            AlkaloidsPresence = true,
                            AlkaloidsMinPercent = 0.1,
                            AlkaloidsMaxPercent = 0.2,
                            MucilagePresence = false,
                            MucilageMinPercent = null,
                            MucilageMaxPercent = null,
                            TanninsPresence = false,
                            TanninsMinPercent = null,
                            TanninsMaxPercent = null,
                            NutrientsPresence = false,
                            NutrientsMinPercent = null,
                            NutrientsMaxPercent = null
                        }
                    })
                }
            }, true));

            // assert
            dynamic data = JsonConvert.DeserializeObject(presenter.ContentResult.Content);
            Assert.Equal((int)HttpStatusCode.OK, presenter.ContentResult.StatusCode);
            Assert.Equal("herb name", data[0].name.Value);
        }

        [Fact]
        public void Handle_GivenSuccessUseCaseResponse_ReturnsMultipleData()
        {
            // arrange
            var presenter = new SearchHerbsPresenter();

            // act
            presenter.Handle(new SearchHerbsResponse(new List<Herb>
            {
                new Herb
                {
                    Name = "herb name1",
                    LatinNames = new List<HerbLatinName>
                    {
                        new HerbLatinName{Name = "latin_name1"}
                    },
                    RussianSynonyms = new List<HerbRussianSynonym>(new[]
                    {
                        new HerbRussianSynonym{Name = "russian synonym1"}
                    }),
                    Parts = new List<HerbPart>(new[]
                    {
                        new HerbPart
                        {
                            AlkaloidsPresence = true,
                            AlkaloidsMinPercent = 0.1,
                            AlkaloidsMaxPercent = 0.2,
                            MucilagePresence = false,
                            MucilageMinPercent = null,
                            MucilageMaxPercent = null,
                            TanninsPresence = false,
                            TanninsMinPercent = null,
                            TanninsMaxPercent = null,
                            NutrientsPresence = false,
                            NutrientsMinPercent = null,
                            NutrientsMaxPercent = null
                        }
                    })
                },
                new Herb
                {
                    Name = "herb name2",
                    LatinNames = new List<HerbLatinName>
                    {
                        new HerbLatinName{Name = "latin_name2"}
                    },
                    RussianSynonyms = new List<HerbRussianSynonym>(new[]
                    {
                        new HerbRussianSynonym{Name = "russian synonym2"}
                    }),
                    Parts = new List<HerbPart>(new[]
                    {
                        new HerbPart
                        {
                            AlkaloidsPresence = true,
                            AlkaloidsMinPercent = 0.4,
                            AlkaloidsMaxPercent = 0.5,
                            MucilagePresence = false,
                            MucilageMinPercent = null,
                            MucilageMaxPercent = null,
                            TanninsPresence = false,
                            TanninsMinPercent = null,
                            TanninsMaxPercent = null,
                            NutrientsPresence = false,
                            NutrientsMinPercent = null,
                            NutrientsMaxPercent = null
                        }
                    })
                }
            }, true));

            // assert
            dynamic data = JsonConvert.DeserializeObject(presenter.ContentResult.Content);
            Assert.Equal((int)HttpStatusCode.OK, presenter.ContentResult.StatusCode);
            Assert.Equal("herb name1", data[0].name.Value);
            Assert.Equal("herb name2", data[1].name.Value);
        }
    }
}