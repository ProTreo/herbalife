### Добавление сущности в `Web.Api.Core`:

| Название           | Использование в документации | Примечание               |
| ------------------ | ---------------------------- | ------------------------ |
| Имя сущности       | `<EntityName>`               |                          |
| Имя таблицы в базе | `<entity_table_name>`        | может иметь `s` на конце |

1. Добавление доменной модели в папке `Domain`

   1. Создать класс `<EntityName>`, наследуемый от класса `BaseEntity`

   2. Добавить классу `<EntityName>` атрибут `[Table("<entity_table_name>")]`

   3. Добавить нужные поля, с публичным геттером и приватным сеттером *(требование EntityFramework)*

      ```c#
      public Param1 Param1 { get; private set; }
      public Param2 Param2 { get; private set; }
      ```

   4. Добавить пустой `internal` конструктор *(требование EntityFramework)*

      ```c#
      internal <EntityName>() {}
      ```

   5. Добавить `internal` конструктор с необходимыми параметрами и проинициализировать их

      ```c#
      internal <EntityName> (Param1 param1, Param2 param2) {
        Param1 = param1;
        Param2 = param2
      }
      ```

2. Добавление перемещаемого объекта в папке `Dto`

   1. При необходимости возвращения ответа о создании сущности в базе в папке `Dto\GatewayResponses\Repositories` создать класс `Create<EntityName>Response`, наследуемый от класса `BaseGatewayResponse`

      1. Добавить поле `Id` типа `string` с публичным геттером

      ```c#
      public string Id { get; }
      ```

      1. Добавить `public` конструктор класса с необходимыми полями и проинициализировать их

      ```c#
      public Create<EntityName>Response(string id, bool success = false, IEnumerable<Error> errors = null) : base(success, errors) {
        Id = id;
      }
      ```

   2. Добавить в папку `Dto\UseCaseResponses` класс `<EntityName>Response`, наследуемый от `UseCaseResponseMessage`

      1. Добавить поле `Id` типа `string` и поле `Errors` типа `IEnumerable<string>` с публичными геттерами

         ```c#
         public string Id { get; }
         public IEnumerable<string> Errors { get; }
         ```

      2. Добавить `public` конструктор успешного ответа для use case, в котором заполняется нужные поля

         ```c#
         public <EntityName>Response(string id, bool success = false, string message = null) : base(success, message) {
           Id = id;
         }
         ```

      3. Добавить `public` конструктор провального ответа для use case, в котором заполняется поле с ошибками

         ```c#
         public <EntityName>Response(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message) {
           Errors = errors;
         }
         ```

   3. Добавить в папку `Dto\UseCaseRequests` класс `<EntityName>Request`, наследуемый от класса `BaseRequest` и интерфейса `IUseCaseRequest<<EntityName>Response>`

      1. Добавить нужные поля в класс с публичными геттерами

         ```c#
         public Param1 Param1 { get; }
         public Param2 Param2 { get; }
         ```

      2. Добавить `public` конструктор запроса для use case с необходимыми полями, полем `ModelStateDictionary modelState` для возможности проводить серверную валидацию и заполнять состояние модели в EntityFramework и проинициализировать их

         ```c#
         public <EntityName>Request(Param1 param1, Param2 param2, ModelStateDictionary modelState) {
           Param1 = param2;
           Param2 = param2;
           ModelState = modelState;
         }
         ```

3. Добавить в папку `Interfaces` необходимые интерфейсы

   1. В папке `Interfaces\Gateways\Repositories` создать интерфейс `I<EntityName>Repository`, наследуемый от интерфейса `IRepository<<EntityName>>`

      1. В случае возможности создания сущности в базе, определить метод

         ```c#
         Task<Create<EntityName>Response> Create(Param1 param1, Param2 param2);
         ```

   2. В папке `Interfaces\UseCases` создать `public` интерфейс `I<EntityName>UseCase`, наследуемый от интерфейса `IUseCaseRequestHandler<<EntityName>Request, <EntityName>Response>`

4. Добавить в папку `UseCases` класс `<EntityName>UseCase`, наследуемый от интерфейса `I<EntityName>UseCase` с необходимой реализацией логики use case, с использованием `I<EntityName>Repository`, проинициализированного через `public` конструктор класса для доступа к БД,

   1. Реализовать метод Handle и передать ответ через outputPort

      ```c#
      private readonly I<EntityName>Repository _<entityName>Repository;
      
      public <EntityName>UseCase(I<EntityName>Repository <entityName>Repository) {
        _<entityName>Repository = <entityName>Repository;
      }
      
      public async Task<bool> Handle(<EntityName>Request message, IOutputPort<<EntityName>Response> outputPort) {
        var response = await _<entityName>Repository.Create(message.Param1, message.Param2);
        
        outputPort.Handle(response.Success
          ? new <EntityName>Response(response.Id, true)
          : new <EntityName>Response(response.Errors.Select(e => e.Description)));
        
        return response.Success;
      }
      ```
   
5. В корневой папке `\` в классе `CoreModule` зарегистрировать интерфейс `I<EntityName>UseCase` как класс `<EntityName>UseCase`

   ```c#
   builder.RegisterType<<EntityName>UseCase>().As<I<EntityName>UseCase>().InstancePerLifetimeScope();
   ```

   

### Добавление репозитория в `Web.Api.Infrastructure`:

1. Добавить в папку `Data\Repositories` класс `<EntityName>Repository`, наследуемый от класса `EfRepository<EntityName>` и интерфейса `I<EntityName>Repository`

2. При необходимости реализовать все нужные методы *(приведен пример для Create)*

   ```c#
   public async Task<Create<EntityName>Response> Create(Param1 param1, Param2 param2) {
     var response = await Add(new <EntityName>(param1, param2));
     
     return new Create<EntityName>Response(response.Id.ToString(), true);
   }
   ```

3. В папке `Data` в классе `AppDbContext` добавить поле для доступа к сущности в БД

   ```c#
   public DbSet<<EntityName>s> { get; set; }
   ```

4. В корневой папке `\` в классе `InfrastructureModule` зарегистрировать интерфейс `I<EntityName>Repository` как класс `<EntityName>Repository`

   ```c#
   builder.RegisterType<<EntityName>Repository>().As<I<EntityName>Repository>().InstancePerLifetimeScope();
   ```

5. Открыть консоль в папке `Web.Api.Infrastructure` и зарегистрировать миграцию, добавляющую новую сущность в БД и обновить базу с помощью команд
   `dotnet ef migrations add <ИмяМиграции> --context AppDbContext`
   `dotnet ef database update --context AppDbContext`

