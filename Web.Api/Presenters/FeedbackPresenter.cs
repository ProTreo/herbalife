﻿using System.Net;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Serialization;

namespace Web.Api.Presenters
{
    public class FeedbackPresenter : IOutputPort<FeedbackResponse>
    {
        public FeedbackPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public JsonContentResult ContentResult { get; }

        public void Handle(FeedbackResponse response)
        {
            ContentResult.StatusCode = (int) (response.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            ContentResult.Content = response.Success
                ? JsonSerializer.SerializeObject(response.Id)
                : JsonSerializer.SerializeObject(response.Message);
        }
    }
}