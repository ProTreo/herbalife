﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public abstract class BaseRequest
    {
        public ModelStateDictionary ModelState { get; set; } = new ModelStateDictionary();
    }
}