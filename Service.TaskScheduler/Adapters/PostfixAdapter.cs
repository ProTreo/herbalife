﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using Microsoft.EntityFrameworkCore.Internal;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.Models;

namespace Service.TaskScheduler.Adapters
{
    internal class PostfixAdapter : IEmailAdapter
    {
        private readonly MailSettings _mailSettings;

        public PostfixAdapter(MailSettings mailSettings)
        {
            _mailSettings = mailSettings;
        }

        public bool Send(EmailData emailData, out List<Error> errors)
        {
            // Заглушка для отключения валидации сертификата
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, policyErrors) => true;

            errors = new List<Error>();

            try
            {
                var client = new SmtpClient
                {
                    Host = _mailSettings.Host,
                    Port = _mailSettings.Port,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(_mailSettings.Username, _mailSettings.Password),
                    EnableSsl = true
                };

                var message = new MailMessage
                {
                    From = new MailAddress(_mailSettings.From),
                    Body = emailData.Message,
                    BodyEncoding = Encoding.UTF8,
                    Subject = emailData.Subject,
                    SubjectEncoding = Encoding.UTF8
                };

                // Добавляем получателей
                foreach (var recipient in emailData.Recipients)
                {
                    message.To.Add(recipient);
                }

                client.Send(message);
            }
            catch (Exception ex)
            {
                errors.Add(new Error("EMAIL", $"{ex}"));
            }

            return !errors.Any();
        }
    }
}