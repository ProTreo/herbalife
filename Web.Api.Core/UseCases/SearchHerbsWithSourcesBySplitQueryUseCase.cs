﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.Specifications;

namespace Web.Api.Core.UseCases
{
    public class SearchHerbsWithSourcesBySplitQueryUseCase : ISearchHerbsUseCase
    {
        private const string CacheKeyAllHerbs = "CACHE_KEY_ALL_HERBS";
        private const char QuerySeparator = ' ';

        private readonly IHerbRepository _herbRepository;
        private readonly IMemoryCache _memoryCache;

        public SearchHerbsWithSourcesBySplitQueryUseCase(IHerbRepository herbRepository, IMemoryCache memoryCache)
        {
            _herbRepository = herbRepository;
            _memoryCache = memoryCache;
        }

        public async Task<bool> Handle(SearchHerbsRequest message, IOutputPort<SearchHerbsResponse> outputPort)
        {
            var specification = new HerbWithSourcesAllSpecification();
            var herbs = await _memoryCache.GetOrCreateAsync(CacheKeyAllHerbs, cacheEntry =>
            {
                cacheEntry.SlidingExpiration = TimeSpan.FromMinutes(1);
                cacheEntry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5);
                cacheEntry.Priority = CacheItemPriority.High;

                return _herbRepository.List(specification);
            });
            var queryParts = message.Query.Split(QuerySeparator).AsQueryable();
            var filteredHerbs = herbs.Where(herb => HerbCheck(herb, queryParts) ||
                                                    HerbLatinNamesCheck(herb.LatinNames, queryParts) ||
                                                    HerbRussianSynonymsCheck(herb.RussianSynonyms, queryParts));

            outputPort.Handle(new SearchHerbsResponse(filteredHerbs, true));

            return true;
        }

        private static bool HerbCheck(Herb herb,
            IQueryable<string> queryParts)
        {
            return herb.Name.StartsWith(queryParts.First(), StringComparison.InvariantCultureIgnoreCase) ||
                   queryParts.Any(qp => herb.Name.Contains(qp, StringComparison.InvariantCultureIgnoreCase));
        }

        private static bool HerbLatinNamesCheck(IEnumerable<HerbLatinName> herbLatinNames,
            IQueryable<string> queryParts)
        {
            return herbLatinNames.Any(hln => hln.Name.StartsWith(queryParts.First(), StringComparison.InvariantCultureIgnoreCase) ||
                                             queryParts.Any(qp => hln.Name.Contains(qp, StringComparison.InvariantCultureIgnoreCase)));
        }

        private static bool HerbRussianSynonymsCheck(IEnumerable<HerbRussianSynonym> herbRussianSynonyms,
            IQueryable<string> queryParts)
        {
            return herbRussianSynonyms.Any(hrs => hrs.Name.StartsWith(queryParts.First(), StringComparison.InvariantCultureIgnoreCase) ||
                                                  queryParts.Any(qp => hrs.Name.Contains(qp, StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}