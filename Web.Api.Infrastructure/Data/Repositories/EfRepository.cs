﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Shared;

namespace Web.Api.Infrastructure.Data.Repositories
{
    public abstract class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly AppDbContext _appDbContext;

        protected EfRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public virtual async Task<T> GetById(int id)
        {
            return await _appDbContext.Set<T>().FindAsync(id);
        }

        public async Task<List<T>> ListAll()
        {
            return await _appDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetSingleBySpec(ISpecification<T> spec)
        {
            var result = await List(spec);
            return result.FirstOrDefault();
        }

        public async Task<List<T>> List(ISpecification<T> spec, int top = -1, bool useCache = true)
        {
            // fetch a Queryable that includes all expression-based includes
            var queryableResultWithIncludes = spec.Includes
                .Aggregate(_appDbContext.Set<T>().AsQueryable(),
                    (current, include) => current.Include(include));

            // modify the IQueryable to include any string-based include statements
            var secondaryResult = spec.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            // return the result of the query using the specification's criteria expression
            var finalQuery = secondaryResult.Where(spec.Criteria);

            // add top clause if specified
            if (top > 0)
            {
                finalQuery = finalQuery
                    .OrderBy(k => k.Id)
                    .Take(top);
            }

            if (!useCache)
            {
                finalQuery = finalQuery.AsNoTracking();
            }

            return await finalQuery.ToListAsync();
        }


        public async Task<T> Add(T entity)
        {
            _appDbContext.Set<T>().Add(entity);
            await _appDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task Update(T entity)
        {
            _appDbContext.Entry(entity).State = EntityState.Modified;
            await _appDbContext.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            _appDbContext.Set<T>().Remove(entity);
            await _appDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Используется для удаления отслеживания всех сущностей в контексте.
        /// </summary>
        public void RemoveAllTrackedEntities()
        {
            foreach (var entityEntry in _appDbContext.ChangeTracker.Entries().ToList())
            {
                if (entityEntry is null)
                {
                    continue;
                }

                entityEntry.State = EntityState.Detached;
            }
        }
    }
}