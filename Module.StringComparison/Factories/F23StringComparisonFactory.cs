﻿using Module.StringComparison.Adapters;

namespace Module.StringComparison.Factories
{
    public class F23StringComparisonFactory : IStringComparisonFactory
    {
        public IStringComparisonAdapter GetJaroWinkler()
        {
            return new F23JaroWinklerAdapter();
        }

        public IStringComparisonAdapter GetNGram(int n)
        {
            return new F23NGramAdapter(n);
        }
    }
}