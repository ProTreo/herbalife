﻿using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Domain.Entities.Feedback;
using Web.Api.Core.Dto.GatewayResponses.Repositories;

namespace Web.Api.Core.Interfaces.Gateways.Repositories
{
    public interface IFeedbackRepository : IRepository<Feedback>
    {
        Task<CreateFeedbackResponse> Create(User user, string message);
        Task<CreateFeedbackResponse> Create(string message);
    }
}