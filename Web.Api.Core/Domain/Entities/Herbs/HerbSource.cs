﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_source")]
    public class HerbSource : BaseEntity
    {
        internal HerbSource()
        {
            ChemicalSourcesToSource = new List<HerbChemicalSourceToHerbSource>();
        }

        public string Type { get; set; }
        public string Author { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Isbn { get; set; }

        [JsonIgnore]
        public virtual ICollection<HerbChemicalSourceToHerbSource> ChemicalSourcesToSource { get; set; }
        public int HerbChemicalSourceId { get; set; }
    }
}