﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Models.Request;
using Web.Api.Models.Settings;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthSettings _authSettings;
        private readonly ExchangeRefreshTokenPresenter _exchangeRefreshTokenPresenter;
        private readonly IExchangeRefreshTokenUseCase _exchangeRefreshTokenUseCase;
        private readonly LoginPresenter _loginPresenter;
        private readonly ILoginUseCase _loginUseCase;

        public AuthController(ILoginUseCase loginUseCase, LoginPresenter loginPresenter,
            IExchangeRefreshTokenUseCase exchangeRefreshTokenUseCase,
            ExchangeRefreshTokenPresenter exchangeRefreshTokenPresenter, IOptions<AuthSettings> authSettings)
        {
            _loginUseCase = loginUseCase;
            _loginPresenter = loginPresenter;
            _exchangeRefreshTokenUseCase = exchangeRefreshTokenUseCase;
            _exchangeRefreshTokenPresenter = exchangeRefreshTokenPresenter;
            _authSettings = authSettings.Value;
        }

        // POST api/auth/login
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            // client validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _loginUseCase.Handle(
                new Core.Dto.UseCaseRequests.LoginRequest(request.Email, request.Password,
                    Request.HttpContext.Connection.RemoteIpAddress?.ToString(), ModelState), _loginPresenter);

            // remote validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            return _loginPresenter.ContentResult;
        }

        // POST api/auth/refreshtoken
        [HttpPost("refreshtoken")]
        public async Task<IActionResult> RefreshToken([FromBody] ExchangeRefreshTokenRequest request)
        {
            // client validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            await _exchangeRefreshTokenUseCase.Handle(
                new Core.Dto.UseCaseRequests.ExchangeRefreshTokenRequest(request.AccessToken, request.RefreshToken,
                    _authSettings.SecretKey, ModelState),
                _exchangeRefreshTokenPresenter);

            // remote validation
            if (!ModelState.IsValid) return BadRequest(ModelState);

            return _exchangeRefreshTokenPresenter.ContentResult;
        }
    }
}