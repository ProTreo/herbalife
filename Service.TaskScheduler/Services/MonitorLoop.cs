﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Service.TaskScheduler.Interfaces;
using Service.TaskScheduler.WorkItems;

namespace Service.TaskScheduler.Services
{
    internal class MonitorLoop
    {
        private readonly CancellationToken _cancellationToken;
        private readonly ILogger _logger;
        private readonly IBackgroundTaskQueue _taskQueue;

        public MonitorLoop(IBackgroundTaskQueue taskQueue, ILogger<MonitorLoop> logger,
            IHostApplicationLifetime applicationLifetime)
        {
            _taskQueue = taskQueue;
            _logger = logger;
            _cancellationToken = applicationLifetime.ApplicationStopping;
        }

        public void StartMonitorLoop()
        {
            _logger.LogInformation("Monitor Loop is starting.");

            Task.Run(Monitor, _cancellationToken);
        }

        public void Monitor()
        {
            while (!_cancellationToken.IsCancellationRequested)
            {
                var keyStroke = Console.ReadKey();

                //_taskQueue.QueueBackgroundWorkItem(token => new WorkItemEmailTaskSend());
            }
        }
    }
}