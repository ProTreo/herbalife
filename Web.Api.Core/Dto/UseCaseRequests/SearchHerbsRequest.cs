﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class SearchHerbsRequest : BaseRequest, IUseCaseRequest<SearchHerbsResponse>
    {
        public SearchHerbsRequest(string query, ModelStateDictionary modelState)
        {
            Query = query;
            ModelState = modelState;
        }

        public string Query { get; }
    }
}