﻿using System.ComponentModel.DataAnnotations;

namespace Web.Api.Core.Domain.Entities.Email
{
    public enum EmailType
    {
        [Display(Name = "Письмо")] Email,

        [Display(Name = "Обратная связь")] Feedback,

        [Display(Name = "Запрос на добавление растения")]
        HerbAddition
    }
}