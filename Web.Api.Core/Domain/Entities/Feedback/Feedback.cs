﻿using System.ComponentModel.DataAnnotations.Schema;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Feedback
{
    [Table("feedbacks")]
    public class Feedback : BaseEntity
    {
        internal Feedback()
        {
        }

        internal Feedback(User user, string message)
        {
            User = user;
            Message = message;
        }

        internal Feedback(string message)
        {
            Message = message;
        }

        public User User { get; private set; }
        public string Message { get; private set; }
    }
}