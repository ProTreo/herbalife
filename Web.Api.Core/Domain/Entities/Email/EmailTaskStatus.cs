﻿using System.ComponentModel.DataAnnotations;

namespace Web.Api.Core.Domain.Entities.Email
{
    public enum EmailTaskStatus
    {
        [Display(Name = "Готово к отправке")]
        Ready,

        [Display(Name = "В очереди")]
        Queue,

        [Display(Name = "Отправлено")]
        Done,

        [Display(Name = "Ошибка отправки")]
        Error
    }
}