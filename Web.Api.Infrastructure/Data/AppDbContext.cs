﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Module.Spreadsheets;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Domain.Entities.Feedback;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Shared;

namespace Web.Api.Infrastructure.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Herb> Herbs { get; set; }
        public DbSet<HerbSource> HerbSources { get; set; }
        public DbSet<HerbPartType> HerbPartTypes { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<HerbAddition> HerbAdditions { get; set; }
        public DbSet<EmailTask> EmailTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(ConfigureUser);
            ConfigureHerbs(modelBuilder);
            ConfigureEmailTasks(modelBuilder);
        }

        private void ConfigureEmailTasks(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmailTask>()
                .Property(et => et.Recipients)
                .HasConversion(
                    r => string.Join(';', r),
                    r => r.Split(';', StringSplitOptions.RemoveEmptyEntries));
        }

        private void ConfigureHerbs(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Herb>().HasIndex(h => h.Name).ForMySqlIsFullText();
            modelBuilder.Entity<HerbLatinName>().HasIndex(hln => hln.Name).ForMySqlIsFullText();
            modelBuilder.Entity<HerbRussianSynonym>().HasIndex(hrs => hrs.Name).ForMySqlIsFullText();
        }

        private void ConfigureUser(EntityTypeBuilder<User> builder)
        {
            var navigation = builder.Metadata.FindNavigation(nameof(User.RefreshTokens));
            //EF access the RefreshTokens collection property through its backing field
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            //builder.Ignore(b => b.Email);
            builder.Ignore(b => b.PasswordHash);
        }

        public override int SaveChanges()
        {
            AddAuitInfo();
            return base.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            AddAuitInfo();
            return await base.SaveChangesAsync();
        }

        private void AddAuitInfo()
        {
            var entries = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity &&
                            (x.State == EntityState.Added || x.State == EntityState.Modified));
            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Added) ((BaseEntity) entry.Entity).Created = DateTime.UtcNow;

                ((BaseEntity) entry.Entity).Modified = DateTime.UtcNow;
            }
        }
    }
}