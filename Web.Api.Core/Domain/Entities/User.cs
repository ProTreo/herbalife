﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities
{
    [Table("users")]
    public class User : BaseEntity
    {
        private readonly List<RefreshToken> _refreshTokens = new List<RefreshToken>();

        internal User()
        {
            /* Required by EF */
        }

        internal User(string identityId, string email)
        {
            IdentityId = identityId;
            Email = email;
            UserName = email;
        }

        public string UserName { get; private set; }
        public string IdentityId { get; private set;}
        public string Email { get; private set;}
        public string PasswordHash { get; private set; }
        public IReadOnlyCollection<RefreshToken> RefreshTokens => _refreshTokens.AsReadOnly();

        public bool HasValidRefreshToken(string refreshToken)
        {
            return _refreshTokens.Any(rt => rt.Token == refreshToken && rt.Active);
        }

        public void AddRefreshToken(string token, int userId, string remoteIpAddress, double daysToExpire = 14)
        {
            _refreshTokens.Add(new RefreshToken(token, DateTime.UtcNow.AddDays(daysToExpire), userId, remoteIpAddress));
        }

        public void RemoveRefreshToken(string refreshToken)
        {
            _refreshTokens.Remove(_refreshTokens.First(t => t.Token == refreshToken));
        }
    }
}