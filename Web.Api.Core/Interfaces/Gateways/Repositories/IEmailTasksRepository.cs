﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Dto.UseCaseResponses;

namespace Web.Api.Core.Interfaces.Gateways.Repositories
{
    public interface IEmailTasksRepository : IRepository<EmailTask>
    {
        Task<CreateEmailTaskResponse> Create(string message, IEnumerable<string> recipients, int entityId, EmailType emailType);
    }
}