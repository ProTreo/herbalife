﻿using Web.Api.Core.Dto;

namespace Web.Api.Models.Response
{
    public class LoginResponse
    {
        public LoginResponse(AccessToken accessToken, string refreshToken)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }

        public AccessToken AccessToken { get; }
        public string RefreshToken { get; }
    }
}