﻿using Web.Api.Core.Domain.Entities.Feedback;

namespace Web.Api.Core.Specifications
{
    public sealed class GetFeedbackByIdSpecification : BaseSpecification<Feedback>
    {
        public GetFeedbackByIdSpecification(int id) : base(f => f.Id == id)
        {
            AddInclude(f => f.User);
        }
    }
}