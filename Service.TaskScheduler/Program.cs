﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Service.TaskScheduler.Models;
using Service.TaskScheduler.Services;
using Web.Api.Core;
using Web.Api.Infrastructure;
using Web.Api.Infrastructure.Data;
using Web.Api.Infrastructure.Identity;

namespace Service.TaskScheduler
{
    public class Program
    {
        private static IServiceProvider _serviceProvider;

        private static readonly CancellationToken _globalCancellationToken = new CancellationToken();
        public static IConfiguration Configuration { get; private set; }

        public static async Task Main(string[] args)
        {
            SetupConfiguration();

            using (var host = Host
                .CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureServices((hostContext, serviceCollection) =>
                {
                    // Add framework services
                    var hostname = Configuration["DBHOST"] ?? "localhost";
                    var port = Configuration["DBPORT"] ?? "3306";
                    var password = Configuration["DBPASSWORD"] ?? "HerbalifePassword2019";

                    serviceCollection.AddDbContext<AppIdentityDbContext>(options => options.UseMySql(
                        $"server={hostname}; userid=herbalife; pwd={password}; port={port}; database=herbalife"));

                    serviceCollection.AddDbContext<AppDbContext>(options => options.UseMySql(
                        $"server={hostname}; userid=herbalife; pwd={password}; port={port}; database=herbalife"));
                    // Конфигурационные классы
                    serviceCollection.Configure<MailSettings>(Configuration.GetSection(nameof(MailSettings)));

                    RegisterServices(serviceCollection);
                })
                .Build())
            {
                await host.StartAsync();

                await _serviceProvider.GetRequiredService<QueuedHostedService>().StartAsync(_globalCancellationToken);
                _serviceProvider.GetRequiredService<EmailSenderLoop>().Start();
                _serviceProvider.GetRequiredService<DatabaseOperationsLoop>().Start();

                await host.WaitForShutdownAsync();
            }
        }

        /// <summary>
        ///     Регистрация модулей для Autofac
        /// </summary>
        /// <param name="serviceCollection"></param>
        private static void RegisterServices(IServiceCollection serviceCollection)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<CoreModule>();
            builder.RegisterModule<InfrastructureModule>();
            builder.RegisterModule<TaskSchedulerModule>();
            //builder.RegisterModule(new InfrastructureModule());
            //builder.RegisterModule(new TaskSchedulerModule());
            builder.Populate(serviceCollection);

            var container = builder.Build();
            _serviceProvider = new AutofacServiceProvider(container);
        }

        /// <summary>
        ///     Настройка конфигурации
        /// </summary>
        private static void SetupConfiguration()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var basePath = AppContext.BaseDirectory;

            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environmentName}.json", true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }
    }
}