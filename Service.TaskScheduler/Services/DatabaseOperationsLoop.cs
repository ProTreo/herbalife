﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Service.TaskScheduler.Interfaces;

namespace Service.TaskScheduler.Services
{
    internal class DatabaseOperationsLoop
    {
        private readonly CancellationToken _cancellationToken;

        private readonly IWorkItem _upsertHerbsWorkItem;
        private readonly ILogger _logger;
        private readonly IBackgroundTaskQueue _taskQueue;

        public DatabaseOperationsLoop(IBackgroundTaskQueue taskQueue, ILogger<DatabaseOperationsLoop> logger,
            IHostApplicationLifetime applicationLifetime, IWorkItemUpsertHerbs upsertHerbsWorkItem)
        {
            _taskQueue = taskQueue;
            _logger = logger;
            _cancellationToken = applicationLifetime.ApplicationStopping;
            _upsertHerbsWorkItem = upsertHerbsWorkItem;
        }

        public void Start()
        {
            _logger.LogInformation("Database Operations Loop is starting.");

            Task.Run(SendEmail, _cancellationToken);
        }

        public async Task SendEmail()
        {
            while (!_cancellationToken.IsCancellationRequested)
            {
                _taskQueue.QueueBackgroundWorkItem(token => _upsertHerbsWorkItem);
                
                await Task.Delay(TimeSpan.FromMinutes(1), _cancellationToken);
            }
        }
    }
}