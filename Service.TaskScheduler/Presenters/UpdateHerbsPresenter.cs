﻿using System.Collections.Generic;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Service.TaskScheduler.Presenters
{
    public class UpdateHerbsPresenter : IOutputPort<UpdateHerbsResponse>
    {
        public bool Success { get; private set; }
        public IEnumerable<Error> Errors { get; private set; }

        public void Handle(UpdateHerbsResponse response)
        {
            Success = response.Success;
            Errors = response.Errors;
        }
    }
}