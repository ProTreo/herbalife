﻿using System.Threading.Tasks;

namespace Service.TaskScheduler.Interfaces
{
    public interface IWorkItem
    {
        Task<IWorkItemResult> Execute();
    }
}