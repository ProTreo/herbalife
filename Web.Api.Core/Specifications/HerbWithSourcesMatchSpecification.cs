﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class HerbWithSourcesMatchSpecification : BaseSpecification<Herb>
    {
        public HerbWithSourcesMatchSpecification(string query) : base(h =>
            EF.Functions.Match(h.Name, $"{query}*", MySqlMatchSearchMode.Boolean)
            || h.LatinNames.Any(hln => EF.Functions.Match(hln.Name, $"{query}*", MySqlMatchSearchMode.Boolean)) ||
            h.RussianSynonyms.Any(hrs => EF.Functions.Match(hrs.Name, $"{query}*", MySqlMatchSearchMode.Boolean)))
        {
            AddInclude(h => h.LatinNames);
            AddInclude(h => h.RussianSynonyms);
            AddInclude(h => h.Parts);
            AddInclude(h => h.Parts);
            AddInclude("Parts.Type");
            AddInclude("Parts.ChemicalSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources.Source");
        }
    }
}