﻿using System.Collections.Generic;
using Service.TaskScheduler.Interfaces;

namespace Service.TaskScheduler.Models
{
    public class WorkItemResult : IWorkItemResult
    {
        public WorkItemResult(bool success = false, IEnumerable<Error> errors = null)
        {
            Success = success;
            Errors = errors;
        }

        public bool Success { get; }
        public IEnumerable<Error> Errors { get; }
    }
}