﻿using F23.StringSimilarity;

namespace Module.StringComparison.Adapters
{
    public class F23JaroWinklerAdapter : IStringComparisonAdapter
    {
        private readonly JaroWinkler _jaroWinkler;

        public F23JaroWinklerAdapter()
        {
            _jaroWinkler = new JaroWinkler();
        }

        public double Similarity(string s1, string s2)
        {
            return _jaroWinkler.Similarity(s1, s2);
        }
    }
}