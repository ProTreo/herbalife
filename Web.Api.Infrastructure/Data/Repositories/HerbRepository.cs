﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Interfaces.Gateways.Repositories;

namespace Web.Api.Infrastructure.Data.Repositories
{
    public class HerbRepository : EfRepository<Herb>, IHerbRepository
    {
        public HerbRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<ICollection<HerbSource>> ListHerbSources()
        {
            return await _appDbContext.HerbSources.ToListAsync();
        }

        public async Task<ICollection<HerbPartType>> ListHerbPartTypes()
        {
            return await _appDbContext.HerbPartTypes.ToListAsync();
        }
    }
}