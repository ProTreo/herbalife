﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Dto.GatewayResponses.Repositories;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public class HerbAdditionUseCase : IHerbAdditionUseCase
    {
        private readonly IEmailTasksRepository _emailTasksRepository;
        private readonly IHerbAdditionRepository _herbAdditionRepository;
        private readonly IUserRepository _userRepository;

        /// <summary>
        ///     Список получателей почтового уведомления типа - Запрос на добавление растения
        /// </summary>
        public IEnumerable<string> _herbAdditionRecipients = new[]
        {
            "help@herbal-lab.ru"
        };

        public HerbAdditionUseCase(IHerbAdditionRepository herbAdditionRepository, IUserRepository userRepository,
            IEmailTasksRepository emailTasksRepository)
        {
            _herbAdditionRepository = herbAdditionRepository;
            _userRepository = userRepository;
            _emailTasksRepository = emailTasksRepository;
        }

        public async Task<bool> Handle(HerbAdditionRequest message, IOutputPort<HerbAdditionResponse> outputPort)
        {
            CreateHerbAdditionResponse response;

            if (!string.IsNullOrEmpty(message.Email))
            {
                var user = await _userRepository.FindByEmail(message.Email);

                if (user == null)
                {
                    message.ModelState.AddModelError(nameof(message.Email), "Почта не зарегистрирована");
                    return false;
                }

                response = await _herbAdditionRepository.Create(user, message.Message);
            }
            else
            {
                response = await _herbAdditionRepository.Create(message.Message);
            }

            // Создаем почтовое уведомление
            if (response.Success)
            {
                await _emailTasksRepository.Create(message.Message, _herbAdditionRecipients, int.Parse(response.Id), EmailType.HerbAddition);
            }

            outputPort.Handle(response.Success
                ? new HerbAdditionResponse(response.Id, true)
                : new HerbAdditionResponse(response.Errors.Select(e => e.Description)));

            return response.Success;
        }
    }
}