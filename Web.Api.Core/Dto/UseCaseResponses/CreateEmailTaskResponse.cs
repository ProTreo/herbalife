﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class CreateEmailTaskResponse : UseCaseResponseMessage
    {
        public CreateEmailTaskResponse(IEnumerable<string> errors, bool success = false, string message = null)
        {
            Errors = errors;
        }

        public CreateEmailTaskResponse(string id, bool success = false, string message = null) : base(success, message)
        {
            Id = id;
        }

        public string Id { get; }
        public IEnumerable<string> Errors { get; }
    }
}