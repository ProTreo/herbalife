﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Module.StringComparison.Adapters;
using Module.StringComparison.Factories;
using Web.Api.Core.Domain.Entities.Herbs;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.Specifications;

namespace Web.Api.Core.UseCases
{
    public class SearchHerbsWithSourcesUsingJaroWinklerUseCase : ISearchHerbsUseCase
    {
        private const int LetterThreshold = 10;
        private const double MaxThreshold = 0.85;
        private const char Separator = ' ';
        private readonly IHerbRepository _herbRepository;
        private readonly IStringComparisonAdapter _stringComparisonAdapter;

        public SearchHerbsWithSourcesUsingJaroWinklerUseCase(IHerbRepository herbRepository, IStringComparisonFactory stringComparisonFactory)
        {
            _herbRepository = herbRepository;

            _stringComparisonAdapter = stringComparisonFactory.GetJaroWinkler();
        }

        public async Task<bool> Handle(SearchHerbsRequest message, IOutputPort<SearchHerbsResponse> outputPort)
        {
            var specification = new HerbWithSourcesAllSpecification();
            var herbs = await _herbRepository.List(specification);
            var filteredHerbs = SearchCollection(herbs, message.Query);

            outputPort.Handle(new SearchHerbsResponse(filteredHerbs, true));

            return true;
        }

        private IEnumerable<Herb> SearchCollection(IEnumerable<Herb> herbs, string query)
        {
            return herbs.Where(h => SearchCondition(h, query, GetThreshold(query.Length)));
        }

        private bool SearchCondition(Herb herb, string query, double threshold)
        {
            threshold = 0.5;

            return query.Split(Separator)
                .Any(queryPart => herb.Name.Split(Separator)
                                      .Select(herbNamePart => _stringComparisonAdapter.Similarity(herbNamePart, queryPart))
                                      .Any(similarity => similarity > threshold) ||
                                  (from herbLatinName in herb.LatinNames
                                      from herbLatinNamePart in herbLatinName.Name.Split(Separator)
                                      select _stringComparisonAdapter.Similarity(herbLatinNamePart, queryPart)).Any(similarity => similarity > threshold) ||
                                  (from herbRussianSynonym in herb.RussianSynonyms
                                      from herbRussianSynonymPart in herbRussianSynonym.Name.Split(Separator)
                                      select _stringComparisonAdapter.Similarity(herbRussianSynonymPart, queryPart)).Any(similarity => similarity > threshold));
        }

        private double GetThreshold(int queryLength)
        {
            if (queryLength > 0 && queryLength < LetterThreshold)
            {
                return queryLength * GetStep();
            }

            return MaxThreshold;
        }

        private double GetStep()
        {
            return MaxThreshold / (LetterThreshold + 1);
        }
    }
}