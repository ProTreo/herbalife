﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Pomelo.EntityFrameworkCore.MySql.Metadata.Internal;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb")]
    public class Herb:BaseEntity
    {
        internal Herb()
        {
            LatinNames = new List<HerbLatinName>();
            RussianSynonyms = new List<HerbRussianSynonym>();
            Parts = new List<HerbPart>();
        }

        public string Name { get; set; }
        public virtual IList<HerbLatinName> LatinNames { get; set; }
        public virtual IList<HerbRussianSynonym> RussianSynonyms { get; set; }
        public virtual IList<HerbPart> Parts { get; set; }
    }
}