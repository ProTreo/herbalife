﻿using Autofac;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.Services;
using Web.Api.Infrastructure.Auth;
using Web.Api.Infrastructure.Data.Repositories;
using Web.Api.Infrastructure.Interfaces;
using Web.Api.Infrastructure.Logging;

namespace Web.Api.Infrastructure
{
    public class InfrastructureModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerDependency();
            builder.RegisterType<HerbRepository>().As<IHerbRepository>().InstancePerDependency();
            builder.RegisterType<FeedbackRepository>().As<IFeedbackRepository>().InstancePerDependency();
            builder.RegisterType<HerbAdditionRepository>().As<IHerbAdditionRepository>().InstancePerDependency();
            builder.RegisterType<EmailTasksRepository>().As<IEmailTasksRepository>().InstancePerDependency();
            builder.RegisterType<JwtFactory>().As<IJwtFactory>().SingleInstance().FindConstructorsWith(new InternalConstructorFinder());
            builder.RegisterType<JwtTokenHandler>().As<IJwtTokenHandler>().SingleInstance().FindConstructorsWith(new InternalConstructorFinder());
            builder.RegisterType<TokenFactory>().As<ITokenFactory>().SingleInstance();
            builder.RegisterType<JwtTokenValidator>().As<IJwtTokenValidator>().SingleInstance().FindConstructorsWith(new InternalConstructorFinder());
            builder.RegisterType<Logger>().As<ILogger>().SingleInstance();
        }
    }
}