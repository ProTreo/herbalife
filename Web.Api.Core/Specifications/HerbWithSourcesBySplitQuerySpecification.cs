﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Specifications
{
    public sealed class HerbWithSourcesBySplitQuerySpecification : BaseSpecification<Herb>
    {
        private const char _separator = ' ';

        public HerbWithSourcesBySplitQuerySpecification(string query) : base(herb => true)
        {
            AddInclude(h => h.LatinNames);
            AddInclude(h => h.RussianSynonyms);
            AddInclude(h => h.Parts);
            AddInclude(h => h.Parts);
            AddInclude("Parts.Type");
            AddInclude("Parts.ChemicalSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources");
            AddInclude("Parts.ChemicalSources.ChemicalSourcesToSources.Source");
        }

        private static Expression<Func<Herb, bool>> BuildExpression(string query)
        {
            var queryParts = query.Split(_separator);

            // STUB
            return herb => true;
        }

        static IQueryable<T> EtForStartsWith<T>(IQueryable<T> query, string propertyValue, MemberInfo propertyInfo)
        {
            var e = Expression.Parameter(typeof(T), "e");
            var m = Expression.MakeMemberAccess(e, propertyInfo);
            var c = Expression.Constant(propertyValue, typeof(string));
            var mi = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
            Expression call = Expression.Call(m, mi, c);

            var lambda = Expression.Lambda<Func<T, bool>>(call, e); 
            return query.Where(lambda);
        }

        private static bool BuildExpressionTree(Herb herb, string query)
        {
            var queryParts = query.Split(_separator).AsQueryable();

            return HerbCheck(herb, queryParts) || HerbLatinNamesCheck(herb.LatinNames, queryParts) || HerbRussianSynonymsCheck(herb.RussianSynonyms, queryParts);
        }

        private static bool HerbCheck(Herb herb, IQueryable<string> queryParts)
        {
            return herb.Name.StartsWith(queryParts.First()) || queryParts.Any(qp => herb.Name.Contains(qp));
        }

        private static bool HerbLatinNamesCheck(IEnumerable<HerbLatinName> herbLatinNames, IQueryable<string> queryParts)
        {
            return herbLatinNames.Any(hln => hln.Name.StartsWith(queryParts.First()) || queryParts.Any(qp => hln.Name.Contains(qp)));
        }

        private static bool HerbRussianSynonymsCheck(IEnumerable<HerbRussianSynonym> herbRussianSynonyms, IQueryable<string> queryParts)
        {
            return herbRussianSynonyms.Any(hrs => hrs.Name.StartsWith(queryParts.First()) || queryParts.Any(qp => hrs.Name.Contains(qp)));
        }
    }
}