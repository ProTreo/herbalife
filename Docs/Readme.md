# Docker

## [не используется] Сборка

Из папки `Web.Api`

1. Запустить `powershell`

2. Собрать контейнер

   | Название             | Описание        |
   | -------------------- | --------------- |
   | herbalife-webapi-dev | Название образа |

   ```powershell
   docker build -t herbalife-webapi-dev .
   ```



---

### Запуск веб-сервиса

```shell
screen -dmSL prod-web-api bash -c 'sudo dotnet run --launch-profile Prod'
screen -dmSL dev-web-api bash -c 'sudo dotnet run'
```

### Запуск планировщика заданий

```shell
screen -dmSL dev-task-scheduler bash -c 'sudo dotnet run'
```



### Создание БД

```sql
CREATE DATABASE herbalife CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

### Создание пользователя в БД

```mysql
CREATE USER 'herbalife'@'localhost' IDENTIFIED BY 'HerbalifePassword2019';
GRANT ALL PRIVILEGES ON herbalife.* TO 'herbalife'@'localhost';
FLUSH PRIVILEGES;
```



### Доступы к БД

| Логин     | Пароль                                                       | Описание                                 |
| --------- | ------------------------------------------------------------ | ---------------------------------------- |
| herbalife | HerbalifePassword2019                                        | Основной пароль на 90% сервисов          |
| root      | HerbalifeRootPassword2019 или **стандартный через разделитель** | Главная почта postfix'а                  |
| help      | HerbalifeHelpPassword2019                                    | Почта обратной связи/помощи              |
| service   | HerbalifeServicePassword2019                                 | Сервисная почта для планировщика заданий |



### Добавление миграции

```powershell
dotnet ef migrations add MIGRATION_NAME --context appdbcontext
dotnet ef migrations add MIGRATION_NAME --context appidentitydbcontext
```



### Обновление БД

```powershell
dotnet ef database update --context appdbcontext
dotnet ef database update --context appidentitydbcontext
```



### Обновление БД до конкретной миграции

```powershell
dotnet ef database update MIGRATION_NAME --context appdbcontext
```



### Скрипты .bashrc

Обновление среды происходит через команду
```bash
source ~/.bash_profile
```

***Обновление и перезапуск бэкенда***

```bash
function rb() {
  echo "---Entering backend directory---"
  cd /home/herbalife/herbalife/ || return
  echo "---Cleaning previous changes---"
  git reset --hard
  git clean -fd
  echo "---Pulling changes from CVS---"
  git pull
  
  echo "---[WEB-API]: Killing old processes---"
  PID_WEB_API=$(ps aux | grep '[S]CREEN -dmSL dev-web-api bash -c sudo dotnet run' | awk '{print $2}')
  kill -9 $PID_WEB_API
  echo "---[WEB-API]: Entering directory---"
  cd /home/herbalife/herbalife/Web.Api/ || return
  echo "---[WEB-API]: Building---"
  sudo dotnet build
  echo "---[WEB-API]: Running---"
  screen -dmSL dev-web-api bash -c 'sudo dotnet run'
  echo "---[WEB-API]: Done---"
  
  echo "---[TASK-SCHEDULER]: Killing old processes---"
  PID_TASK_SCHEDULER=$(ps aux | grep '[S]CREEN -dmSL dev-task-scheduler bash -c sudo dotnet run' | awk '{print $2}')
  kill -9 $PID_TASK_SCHEDULER
  echo "---[TASK-SCHEDULER]: Entering directory---"
  cd /home/herbalife/herbalife/Service.TaskScheduler/ || return
  echo "---[TASK-SCHEDULER]: Building---"
  sudo dotnet build
  echo "---[TASK-SCHEDULER]: Running---"
  screen -dmSL dev-task-scheduler bash -c 'sudo dotnet run'
  echo "---[TASK-SCHEDULER]: Done---"
}
```

***Обновление и перезапуск фронтенда***

```bash
function rf() {
  echo "---Entering frontend directory---"
  cd /home/herbalife/herbal-lab/ || return
  echo "---Cleaning previous changes---"
  git reset --hard
  git clean -fd
  echo "---Pulling changes from CVS---"
  git pull
  echo "---Installing new modules if exist---"
  npm install
  echo "---Redeploing---"
  npm run deploy
  echo "---Done---"
}
```



## Сайт

Файлы из папки `dist` положить в `/ush/share/nginx/html` и выполнить команды ниже

### Права для nginx

```shell
find /usr/share/nginx/ -type d -exec chmod 755 {} \;
find /usr/share/nginx/ -type f -exec chmod 644 {} \
```
