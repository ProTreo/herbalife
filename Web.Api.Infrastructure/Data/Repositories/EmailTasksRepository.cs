﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces.Gateways.Repositories;

namespace Web.Api.Infrastructure.Data.Repositories
{
    public class EmailTasksRepository : EfRepository<EmailTask>, IEmailTasksRepository
    {
        public EmailTasksRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<CreateEmailTaskResponse> Create(string message, IEnumerable<string> recipients, int entityId,
            EmailType emailType)
        {
            var response = await Add(new EmailTask(message, recipients, entityId, emailType));

            return new CreateEmailTaskResponse(response.Id.ToString(), true);
        }
    }
}