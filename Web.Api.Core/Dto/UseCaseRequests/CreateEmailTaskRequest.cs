﻿using System.Collections.Generic;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class CreateEmailTaskRequest : IUseCaseRequest<CreateEmailTaskResponse>
    {
        public CreateEmailTaskRequest(string message, IEnumerable<string> recipients, EmailType type)
        {
            Message = message;
            Recipients = recipients;
            Type = type;
        }

        public string Message { get; }
        public IEnumerable<string> Recipients { get; }
        public EmailType Type { get; }
    }
}