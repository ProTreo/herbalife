﻿using System.Threading.Tasks;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.Specifications;

namespace Web.Api.Core.UseCases
{
    public class SearchHerbsWithSourcesUseCase : ISearchHerbsUseCase
    {
        private readonly IHerbRepository _herbRepository;

        public SearchHerbsWithSourcesUseCase(IHerbRepository herbRepository)
        {
            _herbRepository = herbRepository;
        }

        public async Task<bool> Handle(SearchHerbsRequest message, IOutputPort<SearchHerbsResponse> outputPort)
        {
            var specification = new HerbWithSourcesStartsWithSpecification(message.Query);
            var herbs = await _herbRepository.List(specification);

            outputPort.Handle(new SearchHerbsResponse(herbs, true));

            return true;
        }
    }
}