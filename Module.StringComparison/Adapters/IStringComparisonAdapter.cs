﻿namespace Module.StringComparison.Adapters
{
    public interface IStringComparisonAdapter
    {
        double Similarity(string s1, string s2);
    }
}