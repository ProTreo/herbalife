﻿using FluentValidation;
using Web.Api.Models.Request;

namespace Web.Api.Models.Validation
{
    public class LoginRequestValidator : AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Укажите почту")
                .DependentRules(() =>
                    RuleFor(x => x.Email).EmailAddress().WithMessage("Проверьте адрес")
                        .DependentRules(() => RuleFor(x => x.Password).NotEmpty().WithMessage("Укажите пароль")));
        }
    }
}