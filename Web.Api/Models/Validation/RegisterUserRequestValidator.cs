﻿using FluentValidation;
using Web.Api.Models.Request;

namespace Web.Api.Models.Validation
{
    public class RegisterUserRequestValidator : AbstractValidator<RegisterUserRequest>
    {
        private const int PasswordLengthMin = 8;

        public RegisterUserRequestValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Укажите почту")
                .DependentRules(() =>
                    RuleFor(x => x.Email).EmailAddress().WithMessage("Проверьте адрес")
                        .DependentRules(() => RuleFor(x => x.Password).NotEmpty().WithMessage("Укажите пароль")
                            .DependentRules(() => RuleFor(x => x.Password).MinimumLength(PasswordLengthMin)
                                .WithMessage($"Минимум {PasswordLengthMin} символов")
                                .DependentRules(() =>
                                    RuleFor(x => x.IsPersonalDataProcessingAgreementReceived).Must(x => x).WithMessage("Согласие не получено")))));
        }
    }
}