﻿using System.ComponentModel.DataAnnotations.Schema;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_addition")]
    public class HerbAddition : BaseEntity
    {
        internal HerbAddition()
        {
            /* Required by EF */
        }

        internal HerbAddition(User user, string message)
        {
            User = user;
            Message = message;
        }

        internal HerbAddition(string message)
        {
            Message = message;
        }

        public User User { get; private set; }
        public string Message { get; private set; }
    }
}