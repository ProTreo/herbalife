﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Api.Infrastructure.Migrations.AppDb
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "email_task",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Recipients = table.Column<string>(nullable: true),
                    EntityId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    StatusMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_email_task", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "herb",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "herb_part_type",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_part_type", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "herb_source",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Author = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Year = table.Column<int>(nullable: false),
                    Isbn = table.Column<string>(nullable: true),
                    HerbChemicalSourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_source", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    IdentityId = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "herb_latin_name",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HerbId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_latin_name", x => x.Id);
                    table.ForeignKey(
                        name: "FK_herb_latin_name_herb_HerbId",
                        column: x => x.HerbId,
                        principalTable: "herb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "herb_russian_synonym",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HerbId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_russian_synonym", x => x.Id);
                    table.ForeignKey(
                        name: "FK_herb_russian_synonym_herb_HerbId",
                        column: x => x.HerbId,
                        principalTable: "herb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "herb_part",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    HerbId = table.Column<int>(nullable: false),
                    TypeId = table.Column<int>(nullable: false),
                    AlkaloidsPresence = table.Column<bool>(nullable: true),
                    AlkaloidsMinPercent = table.Column<double>(nullable: true),
                    AlkaloidsMaxPercent = table.Column<double>(nullable: true),
                    MucilagePresence = table.Column<bool>(nullable: true),
                    MucilageMinPercent = table.Column<double>(nullable: true),
                    MucilageMaxPercent = table.Column<double>(nullable: true),
                    TanninsPresence = table.Column<bool>(nullable: true),
                    TanninsMinPercent = table.Column<double>(nullable: true),
                    TanninsMaxPercent = table.Column<double>(nullable: true),
                    NutrientsPresence = table.Column<bool>(nullable: true),
                    NutrientsMinPercent = table.Column<double>(nullable: true),
                    NutrientsMaxPercent = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_part", x => x.Id);
                    table.ForeignKey(
                        name: "FK_herb_part_herb_HerbId",
                        column: x => x.HerbId,
                        principalTable: "herb",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_herb_part_herb_part_type_TypeId",
                        column: x => x.TypeId,
                        principalTable: "herb_part_type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "feedbacks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_feedbacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_feedbacks_users_UserId",
                        column: x => x.UserId,
                        principalTable: "users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "herb_addition",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_addition", x => x.Id);
                    table.ForeignKey(
                        name: "FK_herb_addition_users_UserId",
                        column: x => x.UserId,
                        principalTable: "users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "refresh_tokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RemoteIpAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_refresh_tokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_refresh_tokens_users_UserId",
                        column: x => x.UserId,
                        principalTable: "users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "herb_chemical_source",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    ChemicalType = table.Column<int>(nullable: false),
                    HerbPartId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_chemical_source", x => x.Id);
                    table.ForeignKey(
                        name: "FK_herb_chemical_source_herb_part_HerbPartId",
                        column: x => x.HerbPartId,
                        principalTable: "herb_part",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "herb_chemical_source_to_herb_source",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    HerbChemicalSourceId = table.Column<int>(nullable: false),
                    HerbSourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_herb_chemical_source_to_herb_source", x => x.Id);
                    table.ForeignKey(
                        name: "FK_herb_chemical_source_to_herb_source_herb_chemical_source_Her~",
                        column: x => x.HerbChemicalSourceId,
                        principalTable: "herb_chemical_source",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_herb_chemical_source_to_herb_source_herb_source_HerbSourceId",
                        column: x => x.HerbSourceId,
                        principalTable: "herb_source",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_feedbacks_UserId",
                table: "feedbacks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_addition_UserId",
                table: "herb_addition",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_chemical_source_HerbPartId",
                table: "herb_chemical_source",
                column: "HerbPartId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_chemical_source_to_herb_source_HerbChemicalSourceId",
                table: "herb_chemical_source_to_herb_source",
                column: "HerbChemicalSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_chemical_source_to_herb_source_HerbSourceId",
                table: "herb_chemical_source_to_herb_source",
                column: "HerbSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_latin_name_HerbId",
                table: "herb_latin_name",
                column: "HerbId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_part_HerbId",
                table: "herb_part",
                column: "HerbId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_part_TypeId",
                table: "herb_part",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_herb_russian_synonym_HerbId",
                table: "herb_russian_synonym",
                column: "HerbId");

            migrationBuilder.CreateIndex(
                name: "IX_refresh_tokens_UserId",
                table: "refresh_tokens",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "email_task");

            migrationBuilder.DropTable(
                name: "feedbacks");

            migrationBuilder.DropTable(
                name: "herb_addition");

            migrationBuilder.DropTable(
                name: "herb_chemical_source_to_herb_source");

            migrationBuilder.DropTable(
                name: "herb_latin_name");

            migrationBuilder.DropTable(
                name: "herb_russian_synonym");

            migrationBuilder.DropTable(
                name: "refresh_tokens");

            migrationBuilder.DropTable(
                name: "herb_chemical_source");

            migrationBuilder.DropTable(
                name: "herb_source");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "herb_part");

            migrationBuilder.DropTable(
                name: "herb");

            migrationBuilder.DropTable(
                name: "herb_part_type");
        }
    }
}
