﻿using F23.StringSimilarity;

namespace Module.StringComparison.Adapters
{
    public class F23NGramAdapter : IStringComparisonAdapter
    {
        private readonly NGram _nGram;

        public F23NGramAdapter(int n)
        {
            _nGram = new NGram(n);
        }

        public double Similarity(string s1, string s2)
        {
            return _nGram.Distance(s1, s2);
        }
    }
}