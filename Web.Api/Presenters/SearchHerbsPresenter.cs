﻿using System.Net;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Serialization;

namespace Web.Api.Presenters
{
    public class SearchHerbsPresenter : IOutputPort<SearchHerbsResponse>
    {
        public SearchHerbsPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public JsonContentResult ContentResult { get; }

        public void Handle(SearchHerbsResponse response)
        {
            ContentResult.StatusCode = (int) (response.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            ContentResult.Content = response.Success
                ? JsonSerializer.SerializeObject(response.Herbs)
                : JsonSerializer.SerializeObject(response.Message);
        }
    }
}