﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class UpdateHerbsResponse : UseCaseResponseMessage
    {
        public UpdateHerbsResponse(IEnumerable<Error> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }

        public UpdateHerbsResponse(bool success = false, string message = null) : base(success, message)
        {
        }

        public IEnumerable<Error> Errors { get; set; }
    }
}