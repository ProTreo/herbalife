﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class LoginRequest : BaseRequest, IUseCaseRequest<LoginResponse>
    {
        public LoginRequest(string email, string password, string remoteIpAddress, ModelStateDictionary modelState)
        {
            Email = email;
            Password = password;
            RemoteIpAddress = remoteIpAddress;
            ModelState = modelState;
        }

        public string Email { get; }
        public string Password { get; }
        public string RemoteIpAddress { get; }
    }
}