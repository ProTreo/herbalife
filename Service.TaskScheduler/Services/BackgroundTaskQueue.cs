﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Service.TaskScheduler.Interfaces;

namespace Service.TaskScheduler.Services
{
    public class BackgroundTaskQueue : IBackgroundTaskQueue
    {
        private readonly SemaphoreSlim _signal = new SemaphoreSlim(0);

        private readonly ConcurrentQueue<Func<CancellationToken, IWorkItem>> _workItems =
            new ConcurrentQueue<Func<CancellationToken, IWorkItem>>();

        /// <summary>
        /// Добавляет задание в очередь
        /// </summary>
        /// <param name="workItem">Задание</param>
        public void QueueBackgroundWorkItem(Func<CancellationToken, IWorkItem> workItem)
        {
            if (workItem == null) throw new ArgumentNullException(nameof(workItem));
            
            _workItems.Enqueue(workItem);
            _signal.Release();
        }

        /// <summary>
        /// Снимает задание из очереди
        /// </summary>
        /// <param name="cancellationToken">Токен отмены</param>
        /// <returns></returns>
        public async Task<Func<CancellationToken, IWorkItem>> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }
    }
}