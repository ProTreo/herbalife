﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities.Herbs
{
    [Table("herb_part_type")]
    public class HerbPartType : BaseEntity
    {
        internal HerbPartType()
        {
        }

        public string Name { get; set; }

        //[JsonIgnore]
        //public virtual HerbPart Part { get; set; }
        //public int HerbPartId { get; set; }
    }
}