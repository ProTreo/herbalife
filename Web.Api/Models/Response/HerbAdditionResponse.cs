﻿namespace Web.Api.Models.Response
{
    public class HerbAdditionResponse
    {
        public HerbAdditionResponse(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}