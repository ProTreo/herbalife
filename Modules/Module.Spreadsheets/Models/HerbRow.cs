﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Module.Spreadsheets.Models
{
    public class HerbRow
    {
        private const int AlkaloidsOffset = 4;
        private const int AlkaloidsSourcesOffset = 5;
        private const int MucilageOffset = 6;
        private const int MucilageSourcesOffset = 7;
        private const int TanninsOffset = 8;
        private const int TanninsSourcesOffset = 9;
        private const int NutrientsOffset = 10;
        private const int NutrientsSourcesOffset = 11;

        public HerbRow()
        {
            LatinNames = new List<string>();
            RussianSynonyms = new List<string>();
            AlkaloidsSources = new List<int>();
            MucilageSources = new List<int>();
            TanninsSources = new List<int>();
            NutrientsSources = new List<int>();
        }

        public string Name { get; set; } = string.Empty;
        public List<string> LatinNames { get; set; }
        public List<string> RussianSynonyms { get; set; }
        public string PartName { get; set; } = string.Empty;

        public bool? AlkaloidsPresence { get; set; }
        public double? AlkaloidsMinPercent { get; set; }
        public double? AlkaloidsMaxPercent { get; set; }
        public List<int> AlkaloidsSources { get; set; }

        public bool? MucilagePresence { get; set; }
        public double? MucilageMinPercent { get; set; }
        public double? MucilageMaxPercent { get; set; }
        public List<int> MucilageSources { get; set; }

        public bool? TanninsPresence { get; set; }
        public double? TanninsMinPercent { get; set; }
        public double? TanninsMaxPercent { get; set; }
        public List<int> TanninsSources { get; set; }

        public bool? NutrientsPresence { get; set; }
        public double? NutrientsMinPercent { get; set; }
        public double? NutrientsMaxPercent { get; set; }
        public List<int> NutrientsSources { get; set; }

        public static HerbRow FromObject(IList<object> value)
        {
            var herbRow = new HerbRow();

            if (value.Count > 0) herbRow.Name = value[0].ToString().TrimEnd(Environment.NewLine.ToCharArray());

            if (value.Count > 1)
                herbRow.LatinNames = value[1].ToString().Split(',').Select(ln => ln.Trim().TrimEnd( Environment.NewLine.ToCharArray())).Where(ln => ln != string.Empty).ToList();

            if (value.Count > 2)
                herbRow.RussianSynonyms = value[2].ToString().Split(',').Select(rs => rs.Trim().TrimEnd( Environment.NewLine.ToCharArray())).Where(rs => rs != string.Empty).ToList();

            if (value.Count > 3) herbRow.PartName = value[3].ToString().TrimEnd(Environment.NewLine.ToCharArray());

            if (value.Count > AlkaloidsOffset)
            {
                var alkaloidsValue = value[AlkaloidsOffset].ToString();

                if (alkaloidsValue.Length > 0)
                {
                    herbRow.AlkaloidsPresence = true;

                    var hasPercent = alkaloidsValue.IndexOf('%') > 0;
                    var hasDash = alkaloidsValue.IndexOf('−') > 0;

                    if (hasPercent)
                    {
                        var withoutPercent = alkaloidsValue.Split('%')[0];
                        if (hasDash)
                        {
                            var min = withoutPercent.Split('−')[0];
                            var max = withoutPercent.Split('−')[1];

                            herbRow.AlkaloidsMinPercent = double.Parse(min);
                            herbRow.AlkaloidsMaxPercent = double.Parse(max);
                        }
                        else
                        {
                            herbRow.AlkaloidsMinPercent = double.Parse(withoutPercent);
                            herbRow.AlkaloidsMaxPercent = double.Parse(withoutPercent);
                        }
                    }
                }
            }

            if (value.Count > AlkaloidsSourcesOffset)
            {
                herbRow.AlkaloidsSources = value[AlkaloidsSourcesOffset].ToString().Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToList();
            }

            if (value.Count > MucilageOffset)
            {
                var mucilageValue = value[MucilageOffset].ToString();

                if (mucilageValue.Length > 0)
                {
                    herbRow.MucilagePresence = true;

                    var hasPercent = mucilageValue.IndexOf('%') > 0;
                    var hasDash = mucilageValue.IndexOf('−') > 0;

                    if (hasPercent)
                    {
                        var withoutPercent = mucilageValue.Split('%')[0];
                        if (hasDash)
                        {
                            var min = withoutPercent.Split('−')[0];
                            var max = withoutPercent.Split('−')[1];

                            herbRow.MucilageMinPercent = double.Parse(min);
                            herbRow.MucilageMaxPercent = double.Parse(max);
                        }
                        else
                        {
                            herbRow.MucilageMinPercent = double.Parse(withoutPercent);
                            herbRow.MucilageMaxPercent = double.Parse(withoutPercent);
                        }
                    }
                }
            }

            if (value.Count > MucilageSourcesOffset)
            {
                herbRow.MucilageSources = value[MucilageSourcesOffset].ToString().Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToList();
            }

            if (value.Count > TanninsOffset)
            {
                var tanninsValue = value[TanninsOffset].ToString();

                if (tanninsValue.Length > 0)
                {
                    herbRow.TanninsPresence = true;

                    var hasPercent = tanninsValue.IndexOf('%') > 0;
                    var hasDash = tanninsValue.IndexOf('−') > 0;

                    if (hasPercent)
                    {
                        var withoutPercent = tanninsValue.Split('%')[0];
                        if (hasDash)
                        {
                            var min = withoutPercent.Split('−')[0];
                            var max = withoutPercent.Split('−')[1];

                            herbRow.TanninsMinPercent = double.Parse(min);
                            herbRow.TanninsMaxPercent = double.Parse(max);
                        }
                        else
                        {
                            herbRow.TanninsMinPercent = double.Parse(withoutPercent);
                            herbRow.TanninsMaxPercent = double.Parse(withoutPercent);
                        }
                    }
                }
            }

            if (value.Count > TanninsSourcesOffset)
            {
                herbRow.TanninsSources = value[TanninsSourcesOffset].ToString().Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToList();
            }

            if (value.Count > NutrientsOffset)
            {
                var nutrientsValue = value[NutrientsOffset].ToString();

                if (nutrientsValue.Length > 0)
                {
                    herbRow.NutrientsPresence = true;

                    var hasPercent = nutrientsValue.IndexOf('%') > 0;
                    var hasDash = nutrientsValue.IndexOf('−') > 0;

                    if (hasPercent)
                    {
                        var withoutPercent = nutrientsValue.Split('%')[0];
                        if (hasDash)
                        {
                            var min = withoutPercent.Split('−')[0];
                            var max = withoutPercent.Split('−')[1];

                            herbRow.NutrientsMinPercent = double.Parse(min);
                            herbRow.NutrientsMaxPercent = double.Parse(max);
                        }
                        else
                        {
                            herbRow.NutrientsMinPercent = double.Parse(withoutPercent);
                            herbRow.NutrientsMaxPercent = double.Parse(withoutPercent);
                        }
                    }
                }
            }

            if (value.Count > NutrientsSourcesOffset)
            {
                herbRow.NutrientsSources = value[NutrientsSourcesOffset].ToString().Split(',').Where(s => !string.IsNullOrEmpty(s)).Select(int.Parse).ToList();
            }

            return herbRow;
        }
    }
}