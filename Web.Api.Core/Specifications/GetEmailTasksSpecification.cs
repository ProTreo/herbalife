﻿using Web.Api.Core.Domain.Entities.Email;

namespace Web.Api.Core.Specifications
{
    public sealed class GetEmailTasksSpecification : BaseSpecification<EmailTask>
    {
        public GetEmailTasksSpecification() : base(et => et.Status == EmailTaskStatus.Ready)
        {
        }
    }
}