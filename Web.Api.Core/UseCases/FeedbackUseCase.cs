﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities.Email;
using Web.Api.Core.Dto.GatewayResponses.Repositories;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public class FeedbackUseCase : IFeedbackUseCase
    {
        private readonly IEmailTasksRepository _emailTasksRepository;
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IUserRepository _userRepository;

        /// <summary>
        ///     Список получателей почтового уведомления типа - Обратная связь
        /// </summary>
        public IEnumerable<string> _feedbackRecipients = new[]
        {
            "help@herbal-lab.ru"
        };

        public FeedbackUseCase(IFeedbackRepository feedbackRepository, IUserRepository userRepository,
            IEmailTasksRepository emailTasksRepository)
        {
            _feedbackRepository = feedbackRepository;
            _userRepository = userRepository;
            _emailTasksRepository = emailTasksRepository;
        }

        public async Task<bool> Handle(FeedbackRequest message, IOutputPort<FeedbackResponse> outputPort)
        {
            CreateFeedbackResponse response;

            if (!string.IsNullOrEmpty(message.Email))
            {
                var user = await _userRepository.FindByEmail(message.Email);

                if (user == null)
                {
                    message.ModelState.AddModelError(nameof(message.Email), "Почта не зарегистрирована");
                    return false;
                }

                response = await _feedbackRepository.Create(user, message.Message);
            }
            else
            {
                response = await _feedbackRepository.Create(message.Message);
            }

            // Создаем почтовое уведомление
            if (response.Success)
            {
                await _emailTasksRepository.Create(message.Message, _feedbackRecipients, int.Parse(response.Id), EmailType.Feedback);
            }

            outputPort.Handle(response.Success
                ? new FeedbackResponse(response.Id, true)
                : new FeedbackResponse(response.Errors.Select(e => e.Description)));

            return response.Success;
        }
    }
}