﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities.Herbs;

namespace Web.Api.Core.Interfaces.Gateways.Repositories
{
    public interface IHerbRepository : IRepository<Herb>
    {
        Task<ICollection<HerbSource>> ListHerbSources();
        Task<ICollection<HerbPartType>> ListHerbPartTypes();
    }
}