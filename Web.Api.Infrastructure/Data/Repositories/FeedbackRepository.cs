﻿using System;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Domain.Entities.Feedback;
using Web.Api.Core.Dto.GatewayResponses.Repositories;
using Web.Api.Core.Interfaces.Gateways.Repositories;

namespace Web.Api.Infrastructure.Data.Repositories
{
    public class FeedbackRepository : EfRepository<Feedback>, IFeedbackRepository
    {
        public FeedbackRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<CreateFeedbackResponse> Create(User user, string message)
        {
            var response = await Add(new Feedback(user, message));

            return new CreateFeedbackResponse(response.Id.ToString(), true);
        }

        public async Task<CreateFeedbackResponse> Create(string message)
        {
            var response = await Add(new Feedback(message));

            return new CreateFeedbackResponse(response.Id.ToString(), true);
        }
    }
}
