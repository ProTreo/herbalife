﻿namespace Web.Api.Models.Request
{
    public class FeedbackRequest
    {
        public string Email { get; set; }
        public string Message { get; set; }
    }
}