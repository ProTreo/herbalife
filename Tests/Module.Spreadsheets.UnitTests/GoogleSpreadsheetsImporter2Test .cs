﻿using System.Threading.Tasks;
using Xunit;

namespace Module.Spreadsheets.UnitTests
{
    public class GoogleSpreadsheetsImporter2Test
    {
        [Fact]
        public async Task Read()
        {
            var importer = new GoogleSpreadsheetsImporter2();

            await importer.Import();

            Assert.NotNull(importer.HerbPartTypes);
            Assert.NotNull(importer.HerbSources);
            Assert.NotNull(importer.Herbs);
        }
    }
}